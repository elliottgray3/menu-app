import * as React from 'react';
import { StyleSheet , Linking, TouchableOpacity} from 'react-native';

// import EditScreenInfo from '../components/EditScreenInfo';
//       <EditScreenInfo path="/screens/TabTwoScreen.tsx" />

import { Text, View } from '../components/Themed';

export default function TabFourScreen() {
  return (
    <View style={styles.container}>
      <Text style={[styles.title, {marginBottom: 40}]}>Place Your Order Here</Text>
      <TouchableOpacity onPress={()=> Linking.openURL("https://menu-app-production.herokuapp.com/")} >
        <Text style={styles.buttonText}>https://menu-app-production.herokuapp.com/</Text>
      </TouchableOpacity>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#d2b48c'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  welcome:{
    fontSize: 40,
    fontWeight: 'bold',
    marginBottom: 100
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  butttonContainer: {
    backgroundColor: "#d3d3d3",
    borderWidth: 2, 
    borderColor: '#000000', 
    paddingVertical: 20,
    paddingHorizontal: 100,
    marginTop: 50
  },
  buttonText: {
    textAlign: 'center',
    color: '#0000ff',
    fontWeight: 'bold',
    fontSize: 20
  }
});
