import React, {PureComponent} from 'react';
import { StyleSheet , TouchableOpacity,Image, ScrollView} from 'react-native';

import { Text, View } from '../components/Themed';

const image = require('../images/every-salad.jpg');
const image2 = require('../images/diet-salad.jpg');
const image3 = require('../images/fried-butter.jpg');
const image4 = require('../images/calamari.jpg');
const image5 = require('../images/pot-stickers.jpg');
const image6 = require('../images/baked-soup.jpg');
const image7 = require('../images/plate-shroom.jpg');
const image8 = require('../images/outlander.jpg');
const image9 = require('../images/pizza.png');
const image10 = require('../images/chicken-sandwich.jpg');
const image11 = require('../images/lobster-roll.jpg');
const image12 = require('../images/oyster.jpg');
const image13 = require('../images/baby-chive.jpg');
const image14 = require('../images/monte-cristo.jpg');
const image15 = require('../images/vegan.png');
const image16 = require('../images/shaksh.jpg');
const image17 = require('../images/chick.jpg');
const image18 = require('../images/dorito.jpg');
const image19 = require('../images/steek.jpg');
const image20 = require('../images/mole.jpg');
const image21 = require('../images/pop.jpg');
const image22 = require('../images/orange-chicken.jpg');
const image23 = require('../images/chocolate-chip-cookies.jpg');
const image24 = require('../images/cheesec.jpeg');
const image25 = require('../images/bread.jpg');
const image26 = require('../images/fig.jpg');
const image27 = require('../images/peach.jpg');
const image28 = require('../images/devil.jpg');
const image29 = require('../images/mac.jpg');
const image30 = require('../images/neo.jpg');
const image31 = require('../images/water.jpg');
const image32 = require('../images/cola.jpg');
const image33 = require('../images/coffee.jpg');
const image34 = require('../images/tea.jpg');
const image35 = require('../images/sobe.png');
const image36 = require('../images/gin.jpg');

export default class TabTwoScreen extends PureComponent {
   
     render () {
          let {container, cardText, separator, title, card, cardImage, subText} = styles
          return (
            <ScrollView>        
               <View style={container}>
                  <Text style={styles.title}>Starters</Text>
                 <View style={separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
                 
                 <TouchableOpacity style={card}>
                 <Image source={image} style={cardImage} />
                 <Text style={cardText}>Every Pizza Place Ever Salad</Text>
                 <Text style={subText}>A delicious salad that can be found at any pizzeria. Giant pieces of lettuce, whole tomatoes, two olives, and one pepperoncini. If ordered for delivery, it will be placed in the same bag as the pizza so the letuce wilts!</Text>
                 <Text style={subText}>Contains: Romaine Lettuce, Tomatoes, Olives</Text>
                 <Text style={subText}>$6.99</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image2} style={cardImage} />
                 <Text style={cardText}>I'm on a diet salad</Text>
                 <Text style={subText}>A plain salad that only people who decide to go out to eat with their friends despite being on a diet order. It's served almost instantly and is expensive in order to single you out as the jerk who pretends that eating a variety of foods in moderation isn't an option.</Text>
                 <Text style={subText}>Contains: Romaine Lettuce, Spinach, Cabbage</Text>
                 <Text style={subText}>$10.99</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image3} style={cardImage} />
                 <Text style={cardText}>Fried Butter</Text>
                 <Text style={subText}>The American classic: Fat that is crusted in a coating of breadcrumbs and then fried in heated fat. Served with olive oil and salted butter!</Text>
                 <Text style={subText}>Contains: Butter, Cream Cheese, Peanut Oil, Bread Crumbs</Text>
                 <Text style={subText}>$9.99</Text>
                 </TouchableOpacity>
                 
                 <TouchableOpacity style={card}>
                 <Image source={image4} style={cardImage} />
                 <Text style={cardText}>Calamari</Text>
                 <Text style={subText}>A shareable(if everyone takes 1 bite!) plate of Calamari that is priced more than most entrees for some reason. Served unseasoned because the tears you'll shed after you realize you spent $20 on a single serving appetizer will make it salty enough.</Text>
                 <Text style={subText}>Contains: Squid, Parsley, Vegetable Oil</Text>
                 <Text style={subText}>$6.99</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image5} style={cardImage} />
                 <Text style={cardText}>Pot Stickers</Text>
                 <Text style={subText}>Stick to the pan steamed, Veggie or protien of your choice! They go quick, grab two!</Text>
                 <Text style={subText}>Contains: Cabbage, Pork</Text>
                 <Text style={subText}>$9.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image6} style={cardImage} />
                 <Text style={cardText}>Loaded Baked Potato Soup</Text>
                 <Text style={subText}>By the bowl: Our famous, creamy, loaded, with so much bacon you wont even taste the potato.</Text>
                 <Text style={subText}>Contains: Potato, Cheese, Pork</Text>
                 <Text style={subText}>$4.50</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image7} style={cardImage} />
                 <Text style={cardText}>Plate O'Shroom</Text>
                 <Text style={subText}>Don't like protien? Try this beef substitute, you wont even notice the difference! Smothered with love and flavor.</Text>
                 <Text style={subText}>Contains: Cheese, Mushroom</Text>
                 <Text style={subText}>$15.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image8} style={cardImage} />
                 <Text style={cardText}>The Outlander</Text>
                 <Text style={subText}>Piled high for the crowd. Fired foods and chicken whats more to love? Yes, you may order this as an Entree.</Text>
                 <Text style={subText}>Contains: Chicken, Squid, Cheese, Pepper</Text>
                 <Text style={subText}>$30.00</Text>
                 </TouchableOpacity>

                 <Text style={styles.title}>Entrees</Text>
                 <View style={separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />
                 
                 <TouchableOpacity style={card}>
                 <Image source={image9} style={cardImage} />
                 <Text style={cardText}>Gyro Shop's Pizza</Text>
                 <Text style={subText}>A large slice of pizza that's served in the same way that gyro shops traditionally do. The cheese is somehow over cooked while the dough is still raw in bits. Also, the cheese looks and feels like mozzarella but tastes like a strong cheddar. No one asked to add this to the menu, but we added it anyway.</Text>
                 <Text style={subText}>Contains: Mozzarella, Gluten</Text>
                 <Text style={subText}>$10.00</Text>
                 </TouchableOpacity>


                 <TouchableOpacity style={card}>
                 <Image source={image10} style={cardImage} />
                 <Text style={cardText}>Chicken Sandwich</Text>
                 <Text style={subText}>A chicken sandwich that's drowned in enough sauce that the roll is soggy and the chicken is untasteable. The sauce is delicious, however, so people order these by the bucket. Why not just sell the sauce? No one would pay $13 for 1 serving of sauce...</Text>
                 <Text style={subText}>Contains: Boneless Chicken Breast, Sur La Table Sauce</Text>
                 <Text style={subText}>$13.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image11} style={cardImage} />
                 <Text style={cardText}>Lobster Roll</Text>
                 <Text style={subText}>A hotdog roll with a 1/2 lobster's worth of meat. We poach the lobsters at the start of the day and just soak them in butter. A super easy to prepare meal that we can charge $40 for because it has the buzzword  "Lobster" in its name.</Text>
                 <Text style={subText}>Contains: Lobster, Gluten, Butter</Text>
                 <Text style={subText}>$40.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image12} style={cardImage} />
                 <Text style={cardText}>Rocky Mountain Oysters</Text>
                 <Text style={subText}>Colorado's own rocky mountain oysters! What, you want an appetizer sized portion of a new dish to figure out if you like it without spending $30? Too bad, we have to sell our signature dish in mass volume somehow.</Text>
                 <Text style={subText}>Contains: Oyster</Text>
                 <Text style={subText}>$30.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image13} style={cardImage} />
                 <Text style={cardText}>Baby You Can Chive My Car</Text>
                 <Text style={subText}>A feta stuffed burger on a chive-tastic bun. Topped with a million diced chives and a creamy sour cream & mustard spread.  Comes with fried pickles for wheels… because it’s a car… get it?</Text>
                 <Text style={subText}>Contains: Feta, Chives,  Sour Cream, Mustard, Fried Pickles</Text>
                 <Text style={subText}>$12.99</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image14} style={cardImage} />
                 <Text style={cardText}>Monte Cristo</Text>
                 <Text style={subText}>Ham, turkey, and swiss cheese slapped between two slices of white bread. Dipped in pancake batter, covered in corn cereal, and deep-fried to perfection! Served with raspberry mustard sauce.</Text>
                 <Text style={subText}>Contains: Cheese, Pork, Gluten</Text>
                 <Text style={subText}>$9.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image15} style={cardImage} />
                 <Text style={cardText}>Vegan Food</Text>
                 <Text style={subText}>Avocado and some honey served with nuts. It may be an overpriced, mediocre option, but it lets you not feel guilty for bringing your vegan friends!</Text>
                 <Text style={subText}>Contains: Avocado, Honey, Nuts</Text>
                 <Text style={subText}>$22.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image16} style={cardImage} />
                 <Text style={cardText}>Smoked Shatshuka</Text>
                 <Text style={subText}>Eggs for dinner. Poached in a smokey, spicey tomato sauce that leaves you "feelin' the heat" all night long. Served with challah. </Text>
                 <Text style={subText}>Contains: Egg, Tomato, Magic Smoke</Text>
                 <Text style={subText}>$15.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image17} style={cardImage} />
                 <Text style={cardText}>Chicken Bacon Nuggets</Text>
                 <Text style={subText}>Keto-friendly. Protein packed chicken wrapped in fatty bacon then deep fried to a crisp. Served with bacon-wrapped hotdogs.</Text>
                 <Text style={subText}>Contains: Something</Text>
                 <Text style={subText}>$17.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image18} style={cardImage} />
                 <Text style={cardText}>The Vin Diesel</Text>
                 <Text style={subText}>Overpriced goodness. (Only car people will understand)</Text>
                 <Text style={subText}>Contains: Gluten</Text>
                 <Text style={subText}>$18.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image19} style={cardImage} />
                 <Text style={cardText}>"Hold my _____"</Text>
                 <Text style={subText}>It doesnt matter what it is, just hold it. You are going to need at least two hands. </Text>
                 <Text style={subText}>Contains: Beef</Text>
                 <Text style={subText}>$42.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image20} style={cardImage} />
                 <Text style={cardText}>Enchiladas con mole</Text>
                 <Text style={subText}>Pollo o carne. Your choice rolled in a corn totilla and smothered in our authentic molé. Served with rice and beans. </Text>
                 <Text style={subText}>Contains: Chicken, Pepper, Corn</Text>
                 <Text style={subText}>$25.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image21} style={cardImage} />
                 <Text style={cardText}>Roast Beef Pop-over sandwich</Text>
                 <Text style={subText}>Real comfort food. Beef, slow-roasted until it falls off the bone, loaded on top of a fluffy pop-over bun. Served with mashed potatoes and gravy. </Text>
                 <Text style={subText}>Contains: Gluten, Beef</Text>
                 <Text style={subText}>$30.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image22} style={cardImage} />
                 <Text style={cardText}>The Real Orange Chicken</Text>
                 <Text style={subText}>Bite-sized pieces of chicken, dredged, and fried until golden and crispy. Tossed in a sticky orange sauce. Served with fried rice and chow mein.</Text>
                 <Text style={subText}>Contains: Chicken, Gluten, Peanut</Text>
                 <Text style={subText}>$30.00</Text>
                 </TouchableOpacity>

                 <Text style={styles.title}>Desserts</Text>
                 <View style={separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />

                 <TouchableOpacity style={card}>
                 <Image source={image23} style={cardImage} />
                 <Text style={cardText}>Baked Cookie Dough</Text>
                 <Text style={subText}>Everyone loves cookie dough, so we decided to take it to the next level by baking it! Experience the gooey texture of cookie dough with the crisp exterior that you expect from a cooked product. </Text>
                 <Text style={subText}>Contains: Chocolate Chips, Gluten, Egg</Text>
                 <Text style={subText}>$8.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image24} style={cardImage} />
                 <Text style={cardText}>Blue Berry Cheesecake Wedge</Text>
                 <Text style={subText}>Sweet blueberries and tart lemon. Smooth, dense cheesecake on top of a graham cracker crust. Perfect compliment to any meal. Served by the slice.</Text>
                 <Text style={subText}>Contains: Blueberry, Cheese</Text>
                 <Text style={subText}>$3.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image25} style={cardImage} />
                 <Text style={cardText}>Bread</Text>
                 <Text style={subText}>Europeans love saying that our white bread is cake or a dessert. So, any tourists from europe can try this singular slice of white bread for their dessert option!</Text>
                 <Text style={subText}>Contains: White Bread</Text>
                 <Text style={subText}>$10.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image26} style={cardImage} />
                 <Text style={cardText}>Real Fig Bar</Text>
                 <Text style={subText}>Not the ones you find packaged in the store. Unctious sticky fig jam with a hint of citrusy thyme and lemon. Shoved between a walnut crumble topping and a buttery graham craker crust. This is a seasonal product, so get them while they last!</Text>
                 <Text style={subText}>Contains: Figs, Gluten</Text>
                 <Text style={subText}>$2.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image27} style={cardImage} />
                 <Text style={cardText}>Peach Tatatan Wedge</Text>
                 <Text style={subText}>Much like a fancy pie. These peaches are carmalized in butter with brown suger, vanilla, and star anise. Subtly spicy pink peppercorn crust gives a pop of citrus that will keep you intrigued.</Text>
                 <Text style={subText}>Contains: Something</Text>
                 <Text style={subText}>$3.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image28} style={cardImage} />
                 <Text style={cardText}>Down With the Devil</Text>
                 <Text style={subText}>Deadly Mix of chocolate. Pressed into the form of a cake. Beware!</Text>
                 <Text style={subText}>Contains: Chocolate, Butter, Gluten</Text>
                 <Text style={subText}>$3.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image29} style={cardImage} />
                 <Text style={cardText}>Macarons 6 Ct.</Text>
                 <Text style={subText}>It takes years of practice to master the technique of whipping egg whites into perfect, crispy gluten-free cookies, so you wont mind the price. Then we fill them with American buttercream. You might not get a glucose spike with normal carbs anymore, but you sure will with these cookies! </Text>
                 <Text style={subText}>Contains: Almond</Text>
                 <Text style={subText}>$8.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image30} style={cardImage} />
                 <Text style={cardText}>Mille-Feuille</Text>
                 <Text style={subText}>Sur La Tables' Best Seller!!! Made fresh every morning. These sweet, flakey layers of puff pastry sell out before we open. So if you want this vanilla custard cream filled dessert, please place your order 48 hours in advanced.</Text>
                 <Text style={subText}>Contains: Chocolate, Dairy</Text>
                 <Text style={subText}>$4.00</Text>
                 </TouchableOpacity>

                 <Text style={styles.title}>Drinks</Text>
                 <View style={separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />

                 <TouchableOpacity style={card}>
                 <Image source={image31} style={cardImage} />
                 <Text style={cardText}>Water</Text>
                 <Text style={subText}>Imagine being such a deplorable that you charge for water.</Text>
                 <Text style={subText}>Contains: Water</Text>
                 <Text style={subText}>$0.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image32} style={cardImage} />
                 <Text style={cardText}>Fountain Beverage</Text>
                 <Text style={subText}>Cola products. 16 oz bottles</Text>
                 <Text style={subText}>Contains: Sugar</Text>
                 <Text style={subText}>$2.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image33} style={cardImage} />
                 <Text style={cardText}>Grand Marnier Coffee</Text>
                 <Text style={subText}>We don't have a liquor license. This is nonalcoholic.</Text>
                 <Text style={subText}>Contains: Sugar, Chocolate</Text>
                 <Text style={subText}>$3.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image34} style={cardImage} />
                 <Text style={cardText}>House-Made Iced tea</Text>
                 <Text style={subText}>Made in advanced to steep with various fruits! Best tea you'll ever have</Text>
                 <Text style={subText}>Contains: Tea</Text>
                 <Text style={subText}>$3.00</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image35} style={cardImage} />
                 <Text style={cardText}>Sobe Dragon</Text>
                 <Text style={subText}>Sorry youngin' you'll never get to experience this drink. We serve Knock-off fruit punch</Text>
                 <Text style={subText}>Contains: Cherry, Sugar</Text>
                 <Text style={subText}>$5.99</Text>
                 </TouchableOpacity>

                 <TouchableOpacity style={card}>
                 <Image source={image36} style={cardImage} />
                 <Text style={cardText}>Gin & Tonic</Text>
                 <Text style={subText}>We don't have a liquor license. This is nonalcoholic. Shaken, Not stirred</Text>
                 <Text style={subText}>Contains: Sugar</Text>
                 <Text style={subText}>$3.00</Text>
                 </TouchableOpacity>

               </View>
               </ScrollView>     
               );
     }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#d2b48c'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 50
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
  welcome:{
    fontSize: 40,
    fontWeight: 'bold',
    marginBottom: 70
  },
  cardText: {
     fontSize: 30,
     marginBottom: 10,
  },
  card: {
     backgroundColor: "#fff",
     marginBottom: 10,
     marginLeft: '2%',
     width: '85%',
     shadowColor: '#000', 
     shadowOpacity: 1,
     shadowOffset: {
          width: 3,
          height: 3 
     }
  }, 
  cardImage: {
       width: '100%',
       height: 200,
       resizeMode: 'cover'
  },
  subText: {
    fontSize: 20,
    marginBottom: 10,
  }
});
