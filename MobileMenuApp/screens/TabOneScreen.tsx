import * as React from 'react';
import { StyleSheet} from 'react-native';
import { Text, View } from '../components/Themed';
import { FontAwesome, Feather } from '@expo/vector-icons';
import axios from 'axios'


export default function TabOneScreen() {

  return (
    
    <View style={styles.container}>
      <Text style={styles.welcome}>Welcome to</Text>
      <Text style={styles.welcome}>Sur La Table</Text>
      <View style={styles.separator} lightColor="#eee" darkColor="rgba(255,255,255,0.1)" />

    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#d2b48c'
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  welcome:{
    fontSize: 40,
    fontWeight: 'bold',
    marginBottom: 25
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});
