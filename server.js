const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const nodemailer = require("nodemailer");

const path = require("path");
const { json } = require("body-parser");
const PORT = process.env.PORT || 3001;
require("dotenv").config();

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.set("port", process.env.PORT || 3001);
// Set up for database through mongo.
const MongoClient = require("mongodb").MongoClient;
const url = process.env.MONGO_URI;
const client = new MongoClient(url, { useUnifiedTopology: true });
client.connect();

app.use(express.static(path.join(__dirname, "frontend", "build")));

app.get("*", (req, res) => {
     res.sendFile(path.join(__dirname, "frontend", "build", "index.html"));
});

app.post("/api/paymentInfo", async (req, res, next) => {
     // Fill out the error if we experience issues filling in payment info
     var error = "";
     // We will need the username of the user to get their cart
     const { username, fakeCreditCard, address, phone } = req.body;

     // Connect to the db and query for the users cart
     const database = client.db();
     const results = await database
          .collection("cart")
          .find({ username: username })
          .toArray();

     if (results.length > 0) {
          const results2 = await database.collection("cart").updateOne(
               { username: username },
               {
                    $set: {
                         address: address,
                         phone: phone,
                         fakeCreditCard: fakeCreditCard,
                    },
               }
          );
          error = "Successfully updated payment information.";
     } else {
          error = "Could not find a cart for the specified user.";
     }
     var ret = { error: error };
     res.status(200).json(ret);
});

app.post("/api/fetchCart", async (req, res, next) => {
     // Fill out the error if we experience issues fetching the users cart
     var error = "";
     // We will need the username of the user to get their cart
     const { username } = req.body;
     // Connect to the db and query for the users cart
     const database = client.db();
     const results = await database
          .collection("cart")
          .find({ username: username })
          .toArray();

     var estimatedWait = 0;
     var totalCost = 0.0;
     var foodItems = [];

     if (results.length > 0) {
          estimatedWait = results[0].estimatedWait;
          totalCost = results[0].totalCost;
          // Transfer all foodItems to a separate array
          for (var i = 0; i < results[0].foodItems.length; i++) {
               foodItems.push(results[0].foodItems[i]);
          }

          var ret = {
               estimatedWait: estimatedWait,
               totalCost: totalCost,
               foodItems: foodItems,
               error: error,
          };
          error = "Successfully returned the cart.";
     } else {
          error = "Couldn't find a user with the specified username.";
          var ret = {
               estimatedWait: 0,
               totalCost: 0.0,
               foodItems: foodItems,
               error: error,
          };
     }
     res.status(200).json(ret);
});

app.post("/api/checkout", async (req, res, next) => {
     // Fill out the error if we experience issues checking out
     var error = "";
     // We will receive the username as input
     const { username, email } = req.body;
     // Create databse connection and query the database for the users cart
     const database = client.db();
     const results = await database
          .collection("cart")
          .find({ username: username })
          .toArray();
     var estimatedWait = 0;
     var totalCost = 0.0;
     var foodItems = [];

     if (results.length > 0) {
          estimatedWait = results[0].estimatedWait;
          totalCost = results[0].totalCost;
          // Create a string for each food item and add them to an array.
          for (var i = 0; i < results[0].foodItems.length; i++) {
               var str =
                    results[0].foodItems[i].name +
                    " $" +
                    results[0].foodItems[i].price +
                    " x" +
                    results[0].foodItems[i].quantity +
                    "</br>";
               foodItems.push(str);
          }

          try {
               // Empty the users cart.
               const results2 = await database.collection("cart").updateOne(
                    { username: username },
                    {
                         $set: {
                              totalCost: 0.0,
                              estimatedWait: 0,
                              foodItems: [],
                         },
                    }
               );
               var str = foodItems.join("");
               // Create a message to send in our email.
               const output = `
               <p>Hello ${username},</p>
               <p>Thank you for ordering from Sur La Table.</p>
               <p>Your order will be ready for pick up in approximately ${estimatedWait} minutes.</p>
               <p>Your Order:</p>
               <p>${str}</p>
               <p>Total Cost: $${totalCost}</p>
               `;
               const transporter = nodemailer.createTransport({
                    service: "outlook",
                    port: 587,
                    auth: {
                         user: "largeprojectcop4331@outlook.com",
                         pass: "RLeinecker1!",
                    },
                    tls: {
                         rejectUnauthorized: false,
                    },
               });

               // setup email data with unicode symbols
               let mailOptions = {
                    from: '"Sur La Table"<largeprojectcop4331@outlook.com>', // sender address
                    to: email, // list of receivers
                    subject: "Your Order from Sur La Table.", // Subject line
                    text: "Hello world?", // plain text body
                    html: output, // html body
               };

               // send mail with defined transport object
               transporter.sendMail(mailOptions, (error, info) => {
                    console.log(info);
                    if (error) {
                         return console.log(error);
                    }
                    // console.log('Message sent: %s', info.messageId);
                    // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
               });
               error = "Checkout successful!";
               // Create return JSON with the error if there is one
               var ret = {
                    totalCost: totalCost,
                    estimatedWait: estimatedWait,
                    foodItems: foodItems,
                    error: error,
               };
          } catch (e) {
               error = e.toString();
               // Create return JSON with the error if there is one
               var ret = {
                    totalCost: totalCost,
                    estimatedWait: estimatedWait,
                    foodItems: foodItems,
                    error: error,
               };
          }
     } else {
          error = "Could not find cart for the specified user.";
          var ret = {
               totalCost: totalCost,
               estimatedWait: estimatedWait,
               foodItems: foodItems,
               error: error,
          };
     }

     res.status(200).json(ret);
});

app.post("/api/emailPasswordReset", async (req, res, next) => {
     // Fill out the error if we experience issues sending the password reset email
     var error = "";
     // We will receive the username and email as input
     const { username, email } = req.body;
     // Generate a random string to send in the email
     var randomstring = require("randomstring");
     var token = randomstring.generate();

     const database = client.db();
     // Create connection to databse and query for users with specified username to check if the user exists
     const results = await database
          .collection("customers")
          .find({ username: username })
          .toArray();

     if (results.length > 0) {
          // Set the resetPassword field to the random token
          const results2 = await database
               .collection("customers")
               .updateOne(
                    { username: username },
                    { $set: { resetPassword: token } }
               );
          try {
               // Create a message to send in our email
               const output = `
      <p>Hello ${username},</p>
      <p>Please use the following token and link to reset your password.</p>
      <p>Token: <b>${token}</b></p>
      <p>Link: <a href="${process.env.BASE_URL + "PasswordVerification"}">${
                    process.env.BASE_URL + "PasswordVerification"
               }</a></p>
      `;

               const transporter = nodemailer.createTransport({
                    service: "outlook",
                    port: 587,
                    auth: {
                         user: "largeprojectcop4331@outlook.com",
                         pass: "RLeinecker1!",
                    },
                    tls: {
                         rejectUnauthorized: false,
                    },
               });

               // setup email data with unicode symbols
               let mailOptions = {
                    from: '"Sur La Table"<largeprojectcop4331@outlook.com>', // sender address
                    to: email, // list of receivers
                    subject: "Password reset for Sur La Table.", // Subject line
                    text: "Hello world?", // plain text body
                    html: output, // html body
               };

               // send mail with defined transport object
               transporter.sendMail(mailOptions, (error, info) => {
                    console.log(info);
                    if (error) {
                         return console.log(error);
                    }
                    // console.log('Message sent: %s', info.messageId);
                    // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
               });

               // // create reusable transporter object using the default SMTP transport
               // let transporter = nodemailer.createTransport({
               //      host: "smtp-mail.outlook.com",
               //      port: 587,
               //      secure: false, // true for 465, false for other ports
               //      auth: {
               //           user: "largeprojectcop4331@outlook.com", // generated ethereal user
               //           pass: "RLeinecker1!", // generated ethereal password
               //      },
               // });

               // // send mail with defined transport object
               // let info = await transporter.sendMail({
               //      from: '"Sur Le Table" <largeprojectcop4331@outlook.com>', // sender address
               //      to: email, // list of receivers
               //      subject: "Password reset on Sur Le Table.", // Subject line
               //      text: "Hello world?", // plain text body
               //      html: output, // html body
               // });
               error =
                    "Please check your email to continue resetting your password.";
               // Create return JSON with the error if there is one
               var ret = {
                    error: error,
               };
          } catch (e) {
               error = e.toString();
               // Create return JSON with the error if there is one
               var ret = {
                    error: error,
               };
          }
     } else {
          error = "Account does not exist!";
          var ret = {
               error: error,
          };
     }

     res.status(200).json(ret);
});

app.post("/api/verifyPasswordReset", async (req, res, next) => {
     // Fill out the error if we experience issues verifying an email
     var error = "";
     // Get the resetPassword string from the request
     const { resetPassword, newPassword } = req.body;

     var bcrypt = require("bcryptjs");
     var salt = bcrypt.genSaltSync(10);
     var hashedPass = bcrypt.hashSync(newPassword, salt);

     const database = client.db();
     // Query the database for the resertPassword string
     const results = await database
          .collection("customers")
          .find({ resetPassword: resetPassword })
          .toArray();

     if (results.length > 0) {
          // Update the user's password
          const results2 = await database
               .collection("customers")
               .updateOne(
                    { resetPassword: resetPassword },
                    { $set: { password: hashedPass } }
               );
          error = "Your password has been updated!";
     } else {
          // If there are no results then return an error
          error = "There was an error trying to reset your password.";
     }

     res.status(200).json({ error: error });
});

// API endpoint to verify an email
app.post("/api/verify", async (req, res, next) => {
     // Fill out the error if we experience issues verifying an email
     var error = "";
     // Get the verification string from the request
     const { verificationString } = req.body;
     const database = client.db();
     // Query the database for the verification string
     const results = await database
          .collection("customers")
          .find({ verificationString: verificationString })
          .toArray();

     if (results.length > 0) {
          // Update the user's verified field to true
          const results2 = await database
               .collection("customers")
               .updateOne(
                    { verificationString: verificationString },
                    { $set: { verified: true } }
               );
          error = "Your account has been verified please sign in.";
     } else {
          // If there are no results then return an error
          error = "There was an error verifying your account.";
     }

     res.status(200).json({ error: error });
});

// Endpoint for deleting an item
app.post("/api/deleteItem", async (req, res, next) => {
     // Will fill out this string if we encounter an error
     var error = "";
     // All we need for delete item is the username and the price of that item
     const { username, name, price, waitTime, nutritionInfo } = req.body;

     const database = client.db();
     // First query the cart database for the users cart
     const results = await database
          .collection("cart")
          .find({ username: username })
          .toArray();

     var username2 = "";
     var address = "";
     var phone = "";
     var fakeCreditCard = 0;
     var totalCost = 0.0;
     var estimatedWait = 0;
     var updatedCost = 0.0;
     var updatedTime = 0;

     if (results[0].totalCost == 0 || results[0].estimatedWait == 0) {
          error = "Error, cannot delete because your cart is already empty!";
     } else if (results.length > 0) {
          username2 = results[0].username;
          address = results[0].address;
          phone = results[0].phone;
          fakeCreditCard = results[0].fakeCreditCard;
          totalCost = results[0].totalCost;
          estimatedWait = results[0].estimatedWait;
          updatedCost = totalCost - price;
          updatedTime = estimatedWait - waitTime;
          index = -1;

          // Update the users cost and wait time
          const results2 = await database.collection("cart").updateOne(
               { username: username },
               {
                    $set: {
                         totalCost: updatedCost,
                         estimatedWait: updatedTime,
                    },
               }
          );

          const exists = await database
               .collection("cart")
               .find({ username: username })
               .toArray();

          for (var i = 0; i < exists[0].foodItems.length; i++) {
               // console.log(exists[0].foodItems[i].name);
               if (exists[0].foodItems[i].name === name) {
                    // console.log(exists[0].foodItems[i].name);
                    // console.log(i);
                    index = i;
               }
          }

          quantity = exists[0].foodItems[index].quantity;
          // console.log(exists[0].foodItems[index]);
          // console.log(quantity);
          const results4 = await database.collection("cart").updateOne(
               { username: username, "foodItems.name": name },
               {
                    $set: {
                         "foodItems.$.quantity": quantity - 1,
                    },
               }
          );

          const exists2 = await database
               .collection("cart")
               .find({ username: username })
               .toArray();

          for (var i = 0; i < exists2[0].foodItems.length; i++) {
               // console.log(exists[0].foodItems[i].name);
               if (exists[0].foodItems[i].name === name) {
                    // console.log(exists[0].foodItems[i].name);
                    // console.log(i);
                    index = i;
               }
          }

          quantity = exists2[0].foodItems[index].quantity;

          if (quantity < 1) {
               const results3 = await database.collection("cart").updateOne(
                    { username: username },
                    {
                         $pull: {
                              foodItems: {
                                   name: name,
                              },
                         },
                    }
               );
          }

          // // Update the users cost and wait time
          // const results2 = await database.collection("cart").updateOne(
          //      { username: username },
          //      {
          //           $set: {
          //                totalCost: updatedCost,
          //                estimatedWait: updatedTime,
          //           },
          //      }
          // );
          // const results3 = await database.collection("cart").updateOne(
          //      { username: username },
          //      {
          //           $pull: {
          //                foodItems: {
          //                     name: name,
          //                },
          //           },
          //      }
          // );
     } else {
          error = `Couldn\'t find cart for user: ${username}`;
     }

     var ret = {
          username: username2,
          address: address,
          phone: phone,
          fakeCreditCard: fakeCreditCard,
          totalCost: updatedCost,
          estimatedWait: updatedTime,
          error: error,
     };
     res.status(200).json(ret);
});

// API endpoint to add a menu item
app.post("/api/addItem", async (req, res, next) => {
     // Will fill out this string if we encounter an error
     var error = "";
     // All we need for add item is the username and the price of that item
     const { username, name, price, waitTime, nutritionInfo } = req.body;

     const database = client.db();
     // First query the cart database for the users cart
     const results = await database
          .collection("cart")
          .find({ username: username })
          .toArray();

     var username2 = "";
     var address = "";
     var phone = "";
     var fakeCreditCard = 0;
     var totalCost = 0.0;
     var estimatedWait = 0;
     var updatedCost = 0.0;
     var updatedTime = 0;
     var quantity = 0;
     var index = -1;

     if (results.length > 0) {
          username2 = results[0].username;
          address = results[0].address;
          phone = results[0].phone;
          fakeCreditCard = results[0].fakeCreditCard;
          totalCost = results[0].totalCost;
          estimatedWait = results[0].estimatedWait;
          updatedCost = totalCost + price;
          updatedTime = estimatedWait + waitTime;
          // Update the users cost and wait time
          const results2 = await database.collection("cart").updateOne(
               { username: username },
               {
                    $set: {
                         totalCost: updatedCost,
                         estimatedWait: updatedTime,
                    },
               }
          );

          const exists = await database
               .collection("cart")
               .find({ username: username })
               .toArray();

          for (var i = 0; i < exists[0].foodItems.length; i++) {
               // console.log(exists[0].foodItems[i].name);
               if (exists[0].foodItems[i].name === name) {
                    // console.log(exists[0].foodItems[i].name);
                    // console.log(i);
                    index = i;
               }
          }

          if (index === -1) {
               const results3 = await database.collection("cart").updateOne(
                    { username: username },
                    {
                         $push: {
                              foodItems: {
                                   name: name,
                                   price: price,
                                   waitTime: waitTime,
                                   nutritionInfo: nutritionInfo,
                                   quantity: 1,
                              },
                         },
                    }
               );
          } else {
               quantity = exists[0].foodItems[index].quantity;
               // console.log(exists[0].foodItems[index]);
               // console.log(quantity);
               const results4 = await database.collection("cart").updateOne(
                    { username: username, "foodItems.name": name },
                    {
                         $set: {
                              "foodItems.$.quantity": quantity + 1,
                         },
                    }
               );
          }

          // const results3 = await database.collection("cart").findOneAndUpdate(
          //           {username: username, foodItems: {name: name, price: price,
          //                waitTime: waitTime, nutritionInfo: nutritionInfo}},
          //           { $set : {foodItems: {name: name, price: price, waitTime: waitTime,
          //                nutritionInfo: nutritionInfo}},
          //                $inc: { "foodItems.$.quantity": 1}},
          //           {upsert: true}
          //      );

          // const results3 = await database.collection("cart").updateOne(
          //      { username: username },
          //      {
          //           $push: {
          //                foodItems: {
          //                     name: name,
          //                     price: price,
          //                     waitTime: waitTime,
          //                     nutritionInfo: nutritionInfo,
          //                },
          //           },
          //      }
          // );
     } else {
          error = `Couldn\'t find cart for user: ${username}`;
     }

     var ret = {
          username: username2,
          address: address,
          phone: phone,
          fakeCreditCard: fakeCreditCard,
          totalCost: updatedCost,
          estimatedWait: updatedTime,
     };
     res.status(200).json(ret);
});

app.post("/api/searchInventory", async (req, res, next) => {
     // incoming: userId, search
     // outgoing: results[], error

     var error = "";

     const { name } = req.body;

     var _search = name.trim();

     const database = client.db();
     963;
     const results = await database
          .collection("foodItems")
          .find({ name: { $regex: _search + ".*", $options: "r" } })
          .toArray();

     var _ret = [];
     for (var i = 0; i < results.length; i++) {
          newPrice = "$" + results[i].price;

          _ret.push({
               id: [i + 1],
               category: results[i].category,
               name: results[i].name,
               description: results[i].description,
               price: newPrice,
               nutritionInfo: results[i].nutritionInfo,
               waitTime: results[i].waitTime,
               picture: results[i].picture,
               quantity: results[i].quantity,
          });
     }

     if (_ret.length == 0)
          error = "Error! This item is not on the menu, please try again.";

     var ret = { results: _ret, error: error };
     res.status(200).json(ret);
});

// API endpoint for login
app.post("/api/login", async (req, res, next) => {
     // Error will be filled in if we have an invalid login.
     var error = "";

     const { username, password } = req.body;

     const database = client.db();
     var bcrypt = require("bcryptjs");
     // Create connection to databse and query for users with specified username and password
     const results = await database
          .collection("customers")
          .find({ username: username })
          .toArray();
     // Variables to store results
     var user = "";
     var firstName = "";
     var lastName = "";
     var email = "";

     if (results.length > 0) {
          var resultPass = results[0].password;
          var verified = results[0].verified;
     }

     // Check if the user hasn't been verified
     if (!verified) {
          user = "";
          error = "Your account hasn't been verified, please check your email.";
     } else if (
          results.length > 0 &&
          bcrypt.compareSync(password, resultPass)
     ) {
          user = username;
          firstName = results[0].firstName;
          lastName = results[0].lastName;
          email = results[0].email;
          error = "Successfully logged in.";
     } else {
          user = "";
          error = "Invalid username and password combination.";
     }
     // Create return JSON with the results
     var ret = {
          username: user,
          firstName: firstName,
          lastName: lastName,
          email: email,
          error: error,
     };
     res.status(200).json(ret);
});

// API endpoint for register
app.post("/api/register", async (req, res, next) => {
     // Error will be filled out if there is an issue creating a new user
     var error = "";
     // Process user input
     const {
          firstName,
          lastName,
          username,
          email,
          password,
          address,
          phone,
          fakeCreditCard,
     } = req.body;
     var bcrypt = require("bcryptjs");
     var salt = bcrypt.genSaltSync(10);
     var hashedPass = bcrypt.hashSync(password, salt);
     var randomstring = require("randomstring");
     var token = randomstring.generate();
     var newUser = {
          firstName: firstName,
          lastName: lastName,
          username: username,
          email: email,
          password: hashedPass,
          resetPassword: "",
          verificationString: token,
          verified: false,
     };
     var newCart = {
          username: username,
          address: address,
          phone: phone,
          fakeCreditCard: fakeCreditCard,
          totalCost: 0,
          estimatedWait: 0,
          foodItems: [],
     };

     const database = client.db();
     // Create connection to databse and query for users with specified username for duplicate checking
     const results = await database
          .collection("customers")
          .find({ username: username })
          .toArray();

     if (results.length > 0) {
          error = "Account Creation Failed: Username already exists!";
          var ret = {
               firstName: "",
               lastName: "",
               username: "",
               email: "",
               error: error,
          };
     } else {
          try {
               // Try to insert the new user
               const results = await database
                    .collection("customers")
                    .insertOne(newUser);
               // Create return JSON with the error if there is one
               var ret = {
                    firstName: firstName,
                    lastName: lastName,
                    username: username,
                    email: email,
                    error: error,
               };
               // Create a message to send in our email
               const output = `
        <p>Hello ${username},</p>
        <p>Thank you for registering with Sur La Table. Please use the following token and link to verify your email.</p>
        <p>Token: <b>${token}</b></p>
        <p>Link: <a href="${process.env.BASE_URL + "AccountVerification"}">${
                    process.env.BASE_URL + "AccountVerification"
               }</a></p>
      `;

               const transporter = nodemailer.createTransport({
                    service: "outlook",
                    port: 587,
                    auth: {
                         user: "largeprojectcop4331@outlook.com",
                         pass: "RLeinecker1!",
                    },
                    tls: {
                         rejectUnauthorized: false,
                    },
               });

               // setup email data with unicode symbols
               let mailOptions = {
                    from: '"Sur La Table"<largeprojectcop4331@outlook.com>', // sender address
                    to: email, // list of receivers
                    subject: "Please verify your email for Sur La Table.", // Subject line
                    text: "Hello world?", // plain text body
                    html: output, // html body
               };

               // send mail with defined transport object
               transporter.sendMail(mailOptions, (error, info) => {
                    console.log(info);
                    if (error) {
                         return console.log(error);
                    }
                    // console.log('Message sent: %s', info.messageId);
                    // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
               });

               // // create reusable transporter object using the default SMTP transport
               // let transporter = nodemailer.createTransport({
               //      host: "smtp-mail.outlook.com",
               //      port: 587,
               //      secure: false, // true for 465, false for other ports
               //      auth: {
               //           user: "largeprojectcop4331@outlook.com", // generated ethereal user
               //           pass: "RLeinecker1!", // generated ethereal password
               //      },
               // });

               // // send mail with defined transport object
               // let info = await transporter.sendMail({
               //      from: "largeprojectcop4331@outlook.com", // sender address
               //      to: email, // list of receivers
               //      subject: "Please verify your email for Sur Le Table.", // Subject line
               //      text: "Hello world?", // plain text body
               //      html: output, // html body
               // });

               // // console.log("Message sent: %s", info.messageId);
               // // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

               // // Preview only available when sending through an Ethereal account
               // // console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
               // // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...

               const cartResults = await database
                    .collection("cart")
                    .insertOne(newCart);
               var cartRet = {
                    username: username,
                    address: address,
                    phone: phone,
                    fakeCreditCard: fakeCreditCard,
                    error: error,
               };
          } catch (e) {
               error = e.toString();
               // Create return JSON with the error if there is one
               var ret = {
                    firstName: "",
                    lastName: "",
                    username: "",
                    email: "",
                    error: error,
               };
          }
     }

     res.status(200).json(ret);
});

app.use((req, res, next) => {
     res.setHeader("Access-Control-Allow-Origin", "*");
     res.setHeader(
          "Access-Control-Allow-Headers",
          "Origin, X-Requested-With, Content-Type, Accept, Authorization"
     );
     res.setHeader(
          "Access-Control-Allow-Methods",
          "GET, POST, PATCH, DELETE, OPTIONS"
     );
     next();
});

app.listen(PORT, () => {
     console.log(`Server listening on port ${PORT}.`);
});
