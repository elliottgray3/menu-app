import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import instance from "./axios";
import { Grid } from "@material-ui/core";
import { Alert, AlertTitle } from "@material-ui/lab";
import UserProfile from "./UserProfile";

const ValidatedLoginForm = () => (
     <Formik
          initialValues={{ username: "", password: "" }}
          onSubmit={(values, { setSubmitting }) => {
               setTimeout(() => {
                    instance
                         .post("login", {
                              username: values.username,
                              password: values.password,
                         })
                         .then(function (response) {
                              localStorage.setItem('username', values.username);
                              console.log(response.data.error);
                              console.log(response.status);
                              console.log(response.statusText);
                              console.log(response.headers);
                              console.log(response.config);
                              console.log(response);
                              if (
                                   response.data.error ===
                                   "Successfully logged in."
                              ) {
                                   console.log("Logging in", values.username);
                                   setSubmitting(false);

                                   window.location.href =
                                        "http://www.localhost:3000/Menu";
                              } else if (
                                   response.data.error ===
                                   "Invalid username and password combination."
                              ) {
                                   console.log("Error:", response.data.error);
                                   alert(response.data.error);
                                   setSubmitting(false);
                              } else {
                                   console.log("Error:", response.data.error);
                                   alert(response.data.error);
                                   setSubmitting(false);
                              }
                         });
               });
          }}
          validationSchema={Yup.object().shape({
               password: Yup.string()
                    .required("No password provided.")
                    .min(
                         8,
                         "Password is too short - should be 8 chars minimum."
                    ),
               // .matches(/(?=.*[0-9])/, "Password must contain a number."),
          })}
     >
          {(props) => {
               const {
                    values,
                    touched,
                    errors,
                    isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit,
               } = props;
               return (
                    <form onSubmit={handleSubmit}>
                         <label htmlFor="username">Username</label>
                         <input
                              name="username"
                              type="text"
                              placeholder="Enter your username"
                              value={values.username}
                              onChange={handleChange}
                              onBlur={handleBlur}
                         />
                         <label htmlFor="Password">Password</label>
                         <input
                              name="password"
                              type="password"
                              placeholder="Enter your password"
                              value={values.password}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              className={
                                   errors.password &&
                                   touched.password &&
                                   "error"
                              }
                         />
                         {errors.password && touched.password && (
                              <div className="input-feedback">
                                   {errors.password}
                              </div>
                         )}
                         <button type="submit" disabled={isSubmitting}>
                              Login
                         </button>

                         <hr />
                         <div>
                              <Grid container spacing={2} direction="row">
                                   <Grid item xs>
                                        <a href="/forgotPassword">
                                             Forgot password?
                                        </a>
                                   </Grid>
                                   <Grid
                                        item
                                        xs
                                        alignItems="right"
                                        justify="right"
                                   >
                                        <a href="/RegisterAccount">
                                             Don't have an account? <br />
                                             Register now!
                                        </a>
                                   </Grid>
                              </Grid>
                              <Alert severity="success">
                                   <AlertTitle>Success</AlertTitle>
                                   This is a success alert —{" "}
                                   <strong>check it out!</strong>
                              </Alert>
                         </div>
                    </form>
               );
          }}
     </Formik>
);

export default ValidatedLoginForm;
