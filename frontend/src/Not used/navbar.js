import { Navbar, NavDropdown, Nav } from "react-bootstrap";
import React, { Component } from "react";
import {
     Box,
     Chip,
     Avatar,
     IconButton,
     Badge,
     withStyles,
} from "@material-ui/core";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";

const clickCart = () => {
     return console.log("Cart clicked");
};

const StyledBadge = withStyles((theme) => ({
     badge: {
          right: -3,
          top: 13,
          border: `2px solid ${theme.palette.background.paper}`,
          padding: "0 4px",
     },
}))(Badge);

export default class navbar extends Component {
     render() {
          return (
               <div>
                    <Navbar
                         collapseOnSelect
                         expand="lg"
                         bg="dark"
                         variant="dark"
                    >
                         <Navbar.Brand href="/">Sur Le Table</Navbar.Brand>
                         <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                         <Navbar.Collapse id="responsive-navbar-nav">
                              <Nav className="mr-auto">
                                   <NavDropdown
                                        title="Food Menu"
                                        id="collasible-nav-dropdown"
                                   >
                                        <NavDropdown.Item href="/Menu">
                                             Teasers
                                        </NavDropdown.Item>
                                        <NavDropdown.Item href="/Menu">
                                             Big Teasers
                                        </NavDropdown.Item>
                                        <NavDropdown.Item href="/Menu">
                                             Desserts
                                        </NavDropdown.Item>
                                   </NavDropdown>
                              </Nav>
                              <Nav>
                                   <Box>
                                        <Chip
                                             color="secondary"
                                             label="Username"
                                             deleteIcon={"hi"}
                                             avatar={<Avatar>F</Avatar>}
                                        />
                                        <IconButton
                                             color="secondary"
                                             aria-label="cart"
                                             onClick={clickCart}
                                        >
                                             <StyledBadge
                                                  badgeContent={4}
                                                  color="error"
                                             >
                                                  <ShoppingCartIcon />
                                             </StyledBadge>
                                        </IconButton>
                                   </Box>

                                   <Nav.Link href="/LoginAccount">
                                        Login
                                   </Nav.Link>
                                   <Nav.Link
                                        eventKey={2}
                                        href="/RegisterAccount"
                                   >
                                        Register Account
                                   </Nav.Link>
                              </Nav>
                         </Navbar.Collapse>
                    </Navbar>
               </div>
          );
     }
}
