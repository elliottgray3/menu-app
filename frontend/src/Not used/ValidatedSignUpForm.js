import React from "react";
import * as Yup from "yup";
import instance from "./axios";
import { Formik } from "formik";

import AlertDismissible from "./AlertDismissible";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";

const ValidatedSignUpForm = () => (
     <Formik
          initialValues={{
               username: "",
               firstName: "",
               lastName: "",
               password: "",
               email: "",
          }}
          onSubmit={(values, { setSubmitting }) => {
               setTimeout(() => {
                    instance
                         .post("register", {
                              username: values.username,
                              firstName: values.firstName,
                              lastName: values.lastName,
                              address: "",
                              phone: "",
                              password: values.password,
                              email: values.email,
                              fakeCreditCard: "",
                         })
                         .then(function (response) {
                              console.log(response);
                         });
                    console.log("Checking sign up", values);
                    window.location.href =
                         "http://www.localhost:3000/Verification";

                    setSubmitting(false);
               });
          }}
          validationSchema={Yup.object().shape({
               username: Yup.string().required("Required"),
               email: Yup.string().email().required("Required"),
               password: Yup.string()
                    .required("No password provided.")
                    .min(
                         8,
                         "Password is too short - should be 8 chars minimum."
                    ),
               // .matches(/(?=.*[0-9])/, "Password must contain a number.")
          })}
     >
          {(props) => {
               const {
                    values,
                    touched,
                    errors,
                    isSubmitting,
                    handleChange,
                    handleBlur,
                    handleSubmit,
               } = props;
               return (
                    <form onSubmit={handleSubmit}>
                         <label htmlFor="firstName">First Name</label>
                         <input
                              id="firstName"
                              name="firstName"
                              type="text"
                              placeholder="Enter Firstname"
                              onChange={handleChange}
                              value={values.firstName}
                         />
                         <label htmlFor="lastName">Last Name</label>
                         <input
                              id="lastName"
                              name="lastName"
                              type="text"
                              placeholder="Enter Lastname"
                              onChange={handleChange}
                              value={values.lastName}
                         />
                         <label htmlFor="email">Email Address</label>
                         <input
                              id="email"
                              name="email"
                              type="email"
                              placeholder="Enter your email"
                              onChange={handleChange}
                              value={values.email}
                              onBlur={handleBlur}
                              className={
                                   errors.email && touched.email && "error"
                              }
                         />
                         <label htmlFor="username">Username</label>
                         <input
                              name="username"
                              type="text"
                              placeholder="Enter your username"
                              value={values.username}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              className={
                                   errors.username &&
                                   touched.username &&
                                   "error"
                              }
                         />
                         {errors.username && touched.username && (
                              <div className="input-feedback">
                                   {errors.username}
                              </div>
                         )}
                         <label htmlFor="password">Password</label>
                         <input
                              name="password"
                              type="password"
                              placeholder="Enter your password"
                              value={values.password}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              className={
                                   errors.password &&
                                   touched.password &&
                                   "error"
                              }
                         />
                         {errors.password && touched.password && (
                              <div className="input-feedback">
                                   {errors.password}
                              </div>
                         )}
                         {/* <label htmlFor="address">Address</label>
                         <input
                              id="address"
                              name="address"
                              type="text"
                              placeholder="Enter your address"
                              onChange={handleChange}
                              value={values.address}
                         />
                         <label htmlFor="phone">Phone</label>
                         <PhoneInput
                              inputProps={{
                                   name: "phone",
                                   required: true,
                                   autoFocus: true,
                              }}
                         />

                         <label htmlFor="fakeCreditCard">
                              Credit Card Number
                         </label>
                         <input
                              id="fakeCreditCard"
                              name="fakeCreditCard"
                              type="text"
                              placeholder="Enter your credit card"
                              onChange={handleChange}
                              value={values.fakeCreditCard}
                         /> */}
                         <button type="submit" disabled={isSubmitting}>
                              Sign up
                         </button>
                         <hr />
                    </form>
               );
          }}
     </Formik>
);

export default ValidatedSignUpForm;
