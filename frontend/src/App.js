import "./App.css";
import React from "react";
import Routes from "./Routes";
import TopAppBar from "./components/CartPannel";

function App() {
     return (
          <div>
               <TopAppBar />
               <Routes />
          </div>
     );
}

export default App;
