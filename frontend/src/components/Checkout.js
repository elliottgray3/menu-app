import React from "react";
import Review from "./Review";
import AddressForm from "./AddressForm";
import PaymentForm from "./PaymentForm";
import instance from "../components/axios";
import { cookies } from "../components/Cookies";

import {
     makeStyles,
     CssBaseline,
     AppBar,
     Toolbar,
     Paper,
     Stepper,
     Step,
     StepLabel,
     Button,
     Link,
     Typography,
} from "@material-ui/core";

function Copyright() {
     return (
          <Typography variant="body2" color="textSecondary" align="center">
               {"Copyright © "}
               Sur La Table {}
               {new Date().getFullYear()}
               {"."}
          </Typography>
     );
}

const useStyles = makeStyles((theme) => ({
     appBar: {
          position: "relative",
     },
     layout: {
          width: "auto",
          marginLeft: theme.spacing(2),
          marginRight: theme.spacing(2),
          [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
               width: 600,
               marginLeft: "auto",
               marginRight: "auto",
          },
     },
     paper: {
          marginTop: theme.spacing(3),
          marginBottom: theme.spacing(3),
          padding: theme.spacing(2),
          [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
               marginTop: theme.spacing(6),
               marginBottom: theme.spacing(6),
               padding: theme.spacing(3),
          },
     },
     stepper: {
          padding: theme.spacing(3, 0, 5),
     },
     buttons: {
          display: "flex",
          justifyContent: "flex-end",
     },
     button: {
          marginTop: theme.spacing(3),
          marginLeft: theme.spacing(1),
     },
}));

const steps = ["Payment details", "Review your order"];

function getStepContent(step) {
     switch (step) {
          //case 0:
          //     return <AddressForm />;
          case 0:
               return <PaymentForm />;
          case 1:
               return <Review />;
          default:
               throw new Error("Unknown step");
     }
}

export default function Checkout() {
     const classes = useStyles();
     const [activeStep, setActiveStep] = React.useState(0);

     const handleNext = () => {
          if (localStorage.getItem("username") === null) {
               window.location.href = "/LoginAccount";
          } else {
               setActiveStep(activeStep + 1);

               if (activeStep === 0) {
                    instance
                         .post("paymentInfo", {
                              username: localStorage.getItem("username"),
                              address: localStorage.getItem("address"),
                              fakeCreditCard: localStorage.getItem(
                                   "fakeCreditCard"
                              ),
                              phone: localStorage.getItem("phone"),
                         })
                         .then(function (response) {
                              console.log(response);
                         });
               }
               if (activeStep === 1) {
                    instance
                         .post("checkout", {
                              username: localStorage.getItem("username"),
                              email: localStorage.getItem("email"),
                         })
                         .then(function (response) {
                              console.log(response);

                              cookies.remove("totalprice");
                         });
               }
          }
     };

     const handleBack = () => {
          setActiveStep(activeStep - 1);
     };

     return (
          <React.Fragment>
               <CssBaseline />

               <main className={classes.layout}>
                    <Paper className={classes.paper}>
                         <Typography component="h1" variant="h4" align="center">
                              Checkout
                         </Typography>
                         <Stepper
                              activeStep={activeStep}
                              className={classes.stepper}
                         >
                              {steps.map((label) => (
                                   <Step key={label}>
                                        <StepLabel>{label}</StepLabel>
                                   </Step>
                              ))}
                         </Stepper>
                         <React.Fragment>
                              {activeStep === steps.length ? (
                                   <React.Fragment>
                                        <Typography variant="h5" gutterBottom>
                                             Thank you for your order.
                                        </Typography>
                                        <Typography variant="subtitle1">
                                             Your order number is #2001539. We
                                             have emailed your order
                                             confirmation, and will send you an
                                             update when your order is out for
                                             delivery.
                                        </Typography>
                                   </React.Fragment>
                              ) : (
                                   <React.Fragment>
                                        {getStepContent(activeStep)}
                                        <div className={classes.buttons}>
                                             {activeStep !== 0 && (
                                                  <Button
                                                       onClick={handleBack}
                                                       className={
                                                            classes.button
                                                       }
                                                  >
                                                       Back
                                                  </Button>
                                             )}
                                             <Button
                                                  variant="contained"
                                                  color="primary"
                                                  onClick={handleNext}
                                                  className={classes.button}
                                             >
                                                  {activeStep ===
                                                  steps.length - 1
                                                       ? "Place order"
                                                       : "Next"}
                                             </Button>
                                        </div>
                                   </React.Fragment>
                              )}
                         </React.Fragment>
                    </Paper>
                    <Copyright />
               </main>
          </React.Fragment>
     );
}
