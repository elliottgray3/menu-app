import React from "react";
import Quantity from "./Quantity";
import {
     makeStyles,
     Box,
     IconButton,
     AppBar,
     Toolbar,
     CssBaseline,
     Typography,
     Tooltip,
} from "@material-ui/core";
import Welcome from "./Welcome";

const useStyles = makeStyles((theme) => ({
     root: {
          display: "flex",
     },
     appBar: {
          transition: theme.transitions.create(["margin", "width"], {
               easing: theme.transitions.easing.sharp,
               duration: theme.transitions.duration.leavingScreen,
          }),
     },

     title: {
          flexGrow: 1,
     },
     hide: {
          display: "none",
     },

     content: {
          flexGrow: 1,
          padding: theme.spacing(3),
     },
}));

export default function TopAppBar() {
     const classes = useStyles();

     const goto = () => {
       if (localStorage.getItem("username") === null)
       {
         window.location.href = "/LoginAccount";
       }
       else
       {
          window.location.href = "/cart";
      }
     };


     return (
          <div className={classes.content}>
               <CssBaseline />
               <AppBar
                    color="inherit"
                    position="fixed"
                    className={classes.appBar}
               >
                    <Toolbar>
                         <Typography
                              align="left"
                              color="textPrimary"
                              variant="button"
                              noWrap
                              className={classes.title}
                         >
                              <a href="/">Sur La Table</a>
                         </Typography>
                         <Welcome />
                         <Box>
                              <IconButton
                                   color="inherit"
                                   aria-label="open drawer"
                                   edge="end"
                                   onClick={goto}
                                   className={classes}
                              >
                                   <Quantity />
                              </IconButton>
                         </Box>
                    </Toolbar>
               </AppBar>
               <br />
          </div>
     );
}
