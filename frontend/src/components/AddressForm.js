import React from "react";
import {
     Typography,
     Grid,
     TextField,
     FormControlLabel,
     Checkbox,
} from "@material-ui/core";

export default function AddressForm() {
     return (
          <React.Fragment>
               <Typography variant="h6" gutterBottom>
                    Delivery address
               </Typography>
               <Grid container spacing={3}>
                    <Grid item xs={12} sm={6}>
                         <TextField
                              required
                              id="firstName"
                              name="firstName"
                              label="First name"
                              fullWidth
                              autoComplete="given-name"
                         />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                         <TextField
                              required
                              id="lastName"
                              name="lastName"
                              label="Last name"
                              fullWidth
                              autoComplete="family-name"
                         />
                    </Grid>
                    <Grid item xs={12}>
                         <TextField
                              required
                              id="address1"
                              name="address1"
                              label="Address line 1"
                              fullWidth
                              autoComplete="shipping address-line1"
                         />
                    </Grid>

                    <Grid item xs={12} sm={6}>
                         <TextField
                              required
                              id="city"
                              name="city"
                              label="City"
                              fullWidth
                              autoComplete="shipping address-level2"
                         />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                         <TextField
                              id="state"
                              name="state"
                              label="State/Province/Region"
                              fullWidth
                         />
                    </Grid>
                    <Grid item xs={12} sm={6}>
                         <TextField
                              required
                              id="zip"
                              name="zip"
                              label="Zip / Postal code"
                              fullWidth
                              autoComplete="shipping postal-code"
                         />
                    </Grid>

                    <Grid item xs={12}>
                         <hr />
                         <FormControlLabel
                              control={
                                   <Checkbox
                                        color="secondary"
                                        name="saveAddress"
                                        value="yes"
                                   />
                              }
                              label="Save your address for future deliveries."
                         />
                    </Grid>
               </Grid>
          </React.Fragment>
     );
}
