import React from "react";
import * as Yup from "yup";
import instance from "./axios";
import { withFormik } from "formik";
import {
     withStyles,
     Card,
     CardContent,
     CardActions,
     TextField,
     Button,
     Container,
} from "@material-ui/core";

const styles = () => ({
     card: {
          maxWidth: 420,
          marginTop: 50,
     },
     container: {
          display: "Flex",
          justifyContent: "center",
     },
     actions: {
          float: "right",
     },
});

const form = (props) => {
     const {
          classes,
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit,
          handleReset,
     } = props;

     return (
          <div className={classes.container}>
               <form onSubmit={handleSubmit}>
                    <Card className={classes.card}>
                         <br />
                         <h1>Password reset</h1>
                         <hr />
                         <CardContent>
                              <TextField
                                   id="username"
                                   label="Username"
                                   value={values.username}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   helperText={
                                        touched.username ? errors.username : ""
                                   }
                                   error={
                                        touched.username &&
                                        Boolean(errors.username)
                                   }
                                   margin="dense"
                                   variant="outlined"
                                   fullWidth
                              />
                              <TextField
                                   id="email"
                                   label="Email"
                                   type="email"
                                   value={values.email}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   helperText={
                                        touched.email ? errors.email : ""
                                   }
                                   error={
                                        touched.email && Boolean(errors.email)
                                   }
                                   margin="dense"
                                   variant="outlined"
                                   fullWidth
                              />
                         </CardContent>
                         <Container>
                              <CardActions className={classes.actions}>
                                   <Button
                                        type="submit"
                                        color="primary"
                                        disabled={isSubmitting}
                                   >
                                        SUBMIT
                                   </Button>
                                   <Button
                                        color="secondary"
                                        onClick={handleReset}
                                   >
                                        CLEAR
                                   </Button>
                              </CardActions>
                         </Container>
                    </Card>
               </form>
          </div>
     );
};

const ForgotPassword = withFormik({
     mapPropsToValues: ({ username, email }) => {
          return {
               username: username || "",
               email: email || "",
          };
     },

     validationSchema: Yup.object().shape({
          username: Yup.string().required("Username is required"),

          email: Yup.string()
               .email("Enter a valid email")
               .required("Email is required"),
     }),

     handleSubmit: (values, { setSubmitting }) => {
          setTimeout(() => {
               instance
                    .post("emailPasswordReset", {
                         username: values.username,
                         email: values.email,
                    })
                    .then(function (response) {
                         console.log(response);
                         if (response.data.error === "") {
                              window.location.href = "/PasswordReset";
                              alert("Reset successful");
                         } else if (
                              response.data.error === "Account does not exist!"
                         ) {
                              console.log("Error:", response.data.error);
                              alert(response.data.error);
                         } else {
                              console.log("Error:", response.data.error);
                              alert(response.data.error);
                         }
                    });
               // submit to the server
               // alert(JSON.stringify(values, null, 2));
               setSubmitting(false);
          });
     },
})(form);

export default withStyles(styles)(ForgotPassword);
