import { Button, Chip, Tooltip } from "@material-ui/core";
import { cookies } from "./Cookies";
import React, { Component } from "react";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
export default class Welcome extends Component {
     render() {
          if (
               !cookies.get("username") ||
               cookies.get("username") === "null" ||
               !localStorage.getItem("username")
          ) {
               return (
                    <div>
                         <Button href="/LoginAccount" component={Button}>
                              Login
                         </Button>
                         <Button href="/RegisterAccount" component={Button}>
                              Register
                         </Button>
                    </div>
               );
          } else if (
               cookies.get("username") ||
               localStorage.getItem("username")
          ) {
               return (
                    <div>
                         <Chip
                              color="primary"
                              label={showUsername()}
                              onClick={handleClick}
                              onDelete={logout}
                              deleteIcon={
                                   <Tooltip title="Logout">
                                        <ExitToAppIcon />
                                   </Tooltip>
                              }
                              variant="outlined"
                         />
                    </div>
               );
          }
     }
}

function handleClick() {}

function showUsername() {
     if (!cookies.get("username") || !localStorage.getItem("username"))
          return "null";
     else return localStorage.getItem("username");
}

function logout() {
     cookies.removeChangeListener("username");
     cookies.remove("username");
     localStorage.removeItem("username");
     window.location.href = "/LoginAccount";
}
