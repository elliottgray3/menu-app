import React from "react";
import * as Yup from "yup";
import instance from "./axios";
import { withFormik } from "formik";
import { Alert, AlertTitle } from "@material-ui/lab";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import {
     withStyles,
     Card,
     CardContent,
     CardActions,
     TextField,
     Button,
     Container,
} from "@material-ui/core";
import { cookies } from "./Cookies";

const styles = () => ({
     card: {
          maxWidth: 420,
          marginTop: 50,
     },
     container: {
          display: "Flex",
          justifyContent: "center",
     },
     actions: {
          float: "right",
     },
});

const form = (props) => {
     const {
          classes,
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit,
          handleReset,
     } = props;

     return (
          <div className={classes.container}>
               <form onSubmit={handleSubmit}>
                    <Card className={classes.card}>
                         <br />
                         <h1>Login Now!</h1>
                         <hr />
                         <CardContent>
                              <TextField
                                   id="username"
                                   label="Username"
                                   value={values.username}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   helperText={
                                        touched.username ? errors.username : ""
                                   }
                                   error={
                                        touched.username &&
                                        Boolean(errors.username)
                                   }
                                   margin="dense"
                                   variant="outlined"
                                   fullWidth
                              />
                              <TextField
                                   id="password"
                                   label="Password"
                                   type="password"
                                   value={values.password}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   helperText={
                                        touched.password ? errors.password : ""
                                   }
                                   error={
                                        touched.password &&
                                        Boolean(errors.password)
                                   }
                                   margin="dense"
                                   variant="outlined"
                                   fullWidth
                              />
                         </CardContent>
                         <Container>
                              <CardActions className={classes.actions}>
                                   <Button
                                        type="submit"
                                        color="primary"
                                        disabled={isSubmitting}
                                   >
                                        SUBMIT
                                   </Button>
                                   <Button
                                        color="secondary"
                                        onClick={handleReset}
                                   >
                                        CLEAR
                                   </Button>
                              </CardActions>
                         </Container>
                         <br />
                         <br />
                         <br />
                         <Container>
                              <Alert severity="info" color="info">
                                   <AlertTitle>
                                        Don't have an account?{" "}
                                   </AlertTitle>
                                   <a href="/RegisterAccount">
                                        <strong>Click Here!</strong>
                                   </a>
                              </Alert>
                              <br />
                              <Alert
                                   color="info"
                                   iconMapping={{
                                        info: (
                                             <CheckCircleOutlineIcon fontSize="inherit" />
                                        ),
                                   }}
                                   severity="info"
                              >
                                   <AlertTitle>
                                        Forgot your password?
                                   </AlertTitle>
                                   <a href="/forgotPassword">
                                        <strong>Click Here!</strong>
                                   </a>
                              </Alert>
                         </Container>
                         <br />
                    </Card>
               </form>
          </div>
     );
};

const LoginForm = withFormik({
     mapPropsToValues: ({ username, password }) => {
          return {
               username: username || "",
               password: password || "",
          };
     },

     validationSchema: Yup.object().shape({
          username: Yup.string().required("Username is required"),

          password: Yup.string()
               .min(8, "Password must contain at least 8 characters")
               .required("Enter your password"),
     }),

     handleSubmit: (values, { setSubmitting }) => {
          setTimeout(() => {
               instance
                    .post("login", {
                         username: values.username,
                         password: values.password,
                    })
                    .then(function (response) {
                         if (
                              response.data.error === "Successfully logged in."
                         ) {
                              console.log("Logging in", values.username);
                              setSubmitting(false);
                              localStorage.setItem("username", values.username);
                              localStorage.setItem("email", response.data.email);
                              cookies.set("username", values.username);
                               // Pacman

                              window.location.href = "/Menu";
                         } else if (
                              response.data.error ===
                              "Invalid username and password combination."
                         ) {
                              console.log("Error:", response.data.error);

                              setSubmitting(false);
                         } else {
                              console.log("Error:", response.data.error);

                              setSubmitting(false);
                         }
                    });
               // submit to the server
               setSubmitting(false);
          }, 1000);
     },
})(form);

export default withStyles(styles)(LoginForm);
