import Welcome from "./Welcome";
import React, { Component } from "react";
import { cookies } from "../components/Cookies";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";

import {
     withStyles,
     Box,
     Badge,
     IconButton,
     AppBar,
     Toolbar,
     Typography,
     Tooltip,
} from "@material-ui/core";

export default class Quantity extends Component {
     constructor(props) {
          super(props);
          this.state = {
               data: cookies.get("currentItemAdding"),
          };
     }
     getData() {
          setTimeout(() => {
               console.log("Our data is fetched");
               this.setState({
                    data: cookies.get("currentItemAdding"),
               });
          }, 1000);
     }
     componentDidMount() {
          this.getData();
     }

     render() {
          return (
               <div style={{ display: "flex" }}>
                    <AppBar color="inherit" position="fixed">
                         <Toolbar>
                              <Typography
                                   style={{ flexGrow: 1 }}
                                   align="left"
                                   color="textPrimary"
                                   variant="button"
                                   noWrap
                              >
                                   <a href="/menu">Sur La Table</a>
                              </Typography>
                              <Welcome />
                              <Box>
                                   <Tooltip title="Go to Cart">
                                        <IconButton
                                             color="inherit"
                                             edge="end"
                                             style={{
                                                  flexGrow: 1,
                                                  padding: "20px",
                                             }}
                                        >
                                             <StyledBadge
                                                  badgeContent={this.state.data}
                                                  color="error"
                                             >
                                                  <ShoppingCartIcon />
                                             </StyledBadge>
                                        </IconButton>
                                   </Tooltip>
                              </Box>
                         </Toolbar>
                    </AppBar>
               </div>
          );
     }
}

const StyledBadge = withStyles((theme) => ({
     badge: {
          right: -3,
          top: 13,
          border: `2px solid ${theme.palette.background.paper}`,
          padding: "0 4px",
     },
}))(Badge);
