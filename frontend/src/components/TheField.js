import React from "react";
import * as Yup from "yup";
import instance from "./axios";
import { withFormik } from "formik";
import {
     withStyles,
     Card,
     CardContent,
     CardActions,
     TextField,
     Button,
     Container,
} from "@material-ui/core";


const styles = () => ({
     card: {
          maxWidth: 420,
          minWidth: 350,
          marginTop: 50,
     },
     container: {
          display: "Flex",
          justifyContent: "center",
     },
     actions: {
          float: "right",
     },
});

const form = (props) => {
     const {
          classes,
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit,
          handleReset,
     } = props;
     if (CurrentURL() === "verifyPasswordReset") {
          return (
               <TextField
                    id="newPassword"
                    label="newPassword"
                    value={values.newPassword}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    helperText={touched.newPassword ? errors.newPassword : ""}
                    error={touched.newPassword && Boolean(errors.newPassword)}
                    margin="dense"
                    variant="outlined"
                    fullWidth
               />
          );
     } else {
          return "";
     }
};
const TheField = withFormik({
     mapPropsToValues: ({ username, email }) => {
          return {
               username: username || "",
               email: email || "",
          };
     },

     validationSchema: Yup.object().shape({
          username: Yup.string().required("Username is required"),

          email: Yup.string()
               .email("Enter a valid email")
               .required("Email is required"),
     }),

     handleSubmit: (values, { setSubmitting }) => {
          setTimeout(() => {
               instance
                    .post("emailPasswordReset", {
                         username: values.username,
                         email: values.email,
                    })
                    .then(function (response) {
                         console.log(response);
                         if (response.data.error === "") {
                              window.location.href = "/PasswordReset";
                              alert("Reset successful");
                         } else if (
                              response.data.error === "Account does not exist!"
                         ) {
                              console.log("Error:", response.data.error);
                              alert(response.data.error);
                         } else {
                              console.log("Error:", response.data.error);
                              alert(response.data.error);
                         }
                    });
               // submit to the server
               // alert(JSON.stringify(values, null, 2));
               setSubmitting(false);
          });
     },
})(form);

export default withStyles(styles)(TheField);
function CurrentURL() {
     var urly = window.location.href;
     var token = urly.split("/");
     if (token[3] === "AccountVerification") {
          return "verify";
     } else if (token[3] === "PasswordVerification") {
          return "verifyPasswordReset";
     }
}
