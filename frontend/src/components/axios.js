import axios from "axios";

var instance = axios.create({
  baseURL: "http://menu-app-production.herokuapp.com/api/",
  timeout: 1000,
  headers: { "Content-Type": "application/json" },
});

export default instance;
