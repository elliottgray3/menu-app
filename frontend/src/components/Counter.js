import React from "react";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import RemoveShoppingCartIcon from "@material-ui/icons/RemoveShoppingCart";
import Quantity from "./Quantity";
import instance from "../components/axios";
import Cookies from "universal-cookie";
import {
     makeStyles,
     Box,
     IconButton,
     AppBar,
     Toolbar,
     CssBaseline,
     Typography,
     Tooltip,
} from "@material-ui/core";
import Welcome from "./Welcome";
import { cookies } from "./Cookies";
class Counter extends React.Component {
     state = { count: 1 };

     increment = () => {
          this.setState({
               count: this.state.count + 1,
          });
          cookies.set("currentItemAdding", this.state.count + 1);
     };

     decrement = () => {
          if (this.state.count <= 0) {
               this.setState({
                    count: 0,
               });
               cookies.set("currentItemAdding", 0);
          } else {
               this.setState({
                    count: this.state.count - 1,
               });
               cookies.set("currentItemAdding", this.state.count - 1);
          }
     };
     addItem = (item) => {
          if (localStorage.getItem("username") === null) {
               window.location.href = "/LoginAccount";
          } else {
               // Removing $ from the price for backend processing
               var originalPrice = item.price;

               var newPrice = parseFloat(item.price.slice(1));

               const cookies = new Cookies();
               var total = cookies.get("totalprice");

               if (total === undefined) {
                    total = 0;
               }
               total = parseFloat(total) + parseFloat(newPrice);

               cookies.set("totalprice", total);

               // Convert string values that are supposed to be number values to numbers
               var wait = Number(item.waitTime);
               console.log(item.price);
               var price = Number(item.price.slice(1));
               console.log(price);

               // Reset item.price to original to prevent it from cutting down the number
               item.price = originalPrice;

               instance
                    .post("addItem", {
                         username: localStorage.getItem("username"),
                         name: item.name,
                         price: price,
                         waitTime: wait,
                         nutritionInfo: item.nutritionInfo,
                    })

                    .then(function (response) {
                         console.log(response);
                    });
          }
     };
     deleteItem = (item) => {
          // Removing $ from the price for backend processing
          var originalPrice = item.price;

          item.price = item.price.slice(1);

          // Convert string values that are supposed to be number values to numbers
          var wait = Number(item.waitTime);
          var price = Number(item.price);

          // Reset item.price to original to prevent it from cutting down the number
          item.price = originalPrice;

          instance
               .post("deleteItem", {
                    username: localStorage.getItem("username"),
                    name: item.name,
                    price: price,
                    waitTime: wait,
                    nutritionInfo: item.nutritionInfo,
               })

               .then(function () {
                    window.location.reload();
               });
     };
     render() {
          return (
               <div>
                    <IconButton
                         color="inherit"
                         onClick={(this.increment, this.addItem)}
                         style={{
                              flexGrow: 1,
                              padding: "20px",
                         }}
                    >
                         <Tooltip title="Add item to cart">
                              <AddShoppingCartIcon />
                         </Tooltip>
                    </IconButton>
                    <span>{this.state.count}</span>
                    <IconButton
                         color="inherit"
                         onClick={(this.decrement, this.deleteItem)}
                         style={{
                              flexGrow: 1,
                              padding: "20px",
                         }}
                    >
                         <Tooltip title="Remove from cart">
                              <RemoveShoppingCartIcon>
                                   Delete item
                              </RemoveShoppingCartIcon>
                         </Tooltip>
                    </IconButton>
               </div>
          );
     }
}

export default Counter;
