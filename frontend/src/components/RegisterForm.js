import React from "react";
import * as Yup from "yup";
import instance from "./axios";
import { withFormik } from "formik";
import {
     withStyles,
     Card,
     CardContent,
     CardActions,
     TextField,
     Button,
     Container,
} from "@material-ui/core";
import { Alert, AlertTitle } from "@material-ui/lab";

const styles = () => ({
     card: {
          maxWidth: 420,
          marginTop: 50,
     },
     container: {
          display: "Flex",
          justifyContent: "center",
     },
     actions: {
          float: "right",
     },
});

const form = (props) => {
     const {
          classes,
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit,
          handleReset,
     } = props;

     return (
          <div className={classes.container}>
               <form onSubmit={handleSubmit}>
                    <Card className={classes.card}>
                         <br />
                         <h1>Register Now!</h1>
                         <hr />
                         <CardContent>
                              <TextField
                                   id="firstName"
                                   label="First Name"
                                   value={values.firstName}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   helperText={
                                        touched.firstName
                                             ? errors.firstName
                                             : ""
                                   }
                                   error={
                                        touched.firstName &&
                                        Boolean(errors.firstName)
                                   }
                                   margin="dense"
                                   variant="outlined"
                                   fullWidth
                              />
                              <TextField
                                   id="lastName"
                                   label="Last Name"
                                   value={values.lastName}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   helperText={
                                        touched.lastName ? errors.lastName : ""
                                   }
                                   error={
                                        touched.lastName &&
                                        Boolean(errors.lastName)
                                   }
                                   margin="dense"
                                   variant="outlined"
                                   fullWidth
                              />
                              <TextField
                                   id="email"
                                   label="Email"
                                   type="email"
                                   value={values.email}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   helperText={
                                        touched.email ? errors.email : ""
                                   }
                                   error={
                                        touched.email && Boolean(errors.email)
                                   }
                                   margin="dense"
                                   variant="outlined"
                                   fullWidth
                              />
                              <TextField
                                   id="username"
                                   label="Username"
                                   value={values.username}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   helperText={
                                        touched.username ? errors.username : ""
                                   }
                                   error={
                                        touched.username &&
                                        Boolean(errors.username)
                                   }
                                   margin="dense"
                                   variant="outlined"
                                   fullWidth
                              />
                              <TextField
                                   id="password"
                                   label="Password"
                                   type="password"
                                   value={values.password}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   helperText={
                                        touched.password ? errors.password : ""
                                   }
                                   error={
                                        touched.password &&
                                        Boolean(errors.password)
                                   }
                                   margin="dense"
                                   variant="outlined"
                                   fullWidth
                              />
                              <TextField
                                   id="confirmPassword"
                                   label="Confirm Password"
                                   type="password"
                                   value={values.confirmPassword}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   helperText={
                                        touched.confirmPassword
                                             ? errors.confirmPassword
                                             : ""
                                   }
                                   error={
                                        touched.confirmPassword &&
                                        Boolean(errors.confirmPassword)
                                   }
                                   margin="dense"
                                   variant="outlined"
                                   fullWidth
                              />
                         </CardContent>
                         <Container>
                              <CardActions className={classes.actions}>
                                   <Button
                                        type="submit"
                                        color="primary"
                                        disabled={isSubmitting}
                                   >
                                        SUBMIT
                                   </Button>
                                   <Button
                                        color="secondary"
                                        onClick={handleReset}
                                   >
                                        CLEAR
                                   </Button>
                              </CardActions>
                         </Container>
                         <br />
                         <br />
                         <br />
                         <Container>
                              <Alert severity="info" color="info">
                                   <AlertTitle>
                                        Already have an account?{" "}
                                   </AlertTitle>
                                   <a href="/LoginAccount">
                                        <strong>Click Here!</strong>
                                   </a>
                              </Alert>
                         </Container>
                         <br />
                    </Card>
               </form>
          </div>
     );
};

const RegisterForm = withFormik({
     mapPropsToValues: ({
          username,
          firstName,
          lastName,
          email,
          password,
          confirmPassword,
     }) => {
          return {
               username: username || "",
               firstName: firstName || "",
               lastName: lastName || "",
               email: email || "",
               password: password || "",
               confirmPassword: confirmPassword || "",
          };
     },

     validationSchema: Yup.object().shape({
          username: Yup.string().required("Username is required"),
          firstName: Yup.string().required("Required"),
          lastName: Yup.string().required("Required"),
          email: Yup.string()
               .email("Enter a valid email")
               .required("Email is required"),
          password: Yup.string()
               .min(8, "Password must contain at least 8 characters")
               .required("Enter your password"),
          confirmPassword: Yup.string()
               .required("Confirm your password")
               .oneOf([Yup.ref("password")], "Password does not match"),
     }),

     handleSubmit: (values, { setSubmitting }) => {
          setTimeout(() => {
               instance
                    .post("register", {
                         username: values.username,
                         firstName: values.firstName,
                         lastName: values.lastName,
                         address: "",
                         phone: "",
                         password: values.password,
                         email: values.email,
                         fakeCreditCard: "",
                    })
                    .then(function (response) {
                         console.log(response);
                         if (response.data.error === "") {
                              window.location.href = "/AccountVerification";
                              alert("Sign up successful");
                         } else if (
                              response.data.error ===
                              "Account Creation Failed: Username already exists!"
                         ) {
                              console.log("Error:", response.data.error);
                              alert(response.data.error);
                         } else {
                              console.log("Error:", response.data.error);
                              alert(response.data.error);
                         }
                    });
               // submit to the server
               // alert(JSON.stringify(values, null, 2));
               setSubmitting(false);
          });
     },
})(form);

export default withStyles(styles)(RegisterForm);
