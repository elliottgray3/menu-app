
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import React, { Component } from "react";

export default class PaymentForm extends Component{

  constructor(props) {
    super(props);
    this.state =
    {
      address: '',
      fakeCreditCard: '',
      phone: '',
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleCard = this.handleCard.bind(this);
    this.handlePhone = this.handlePhone.bind(this);
  }


handleChange (event){

  this.setState({address: event.target.value})
  localStorage.setItem("address", event.target.value);
}
handleCard (event){

  this.setState({fakeCreditCard: event.target.value})
  localStorage.setItem("fakeCreditCard", event.target.value);
}
handlePhone (event){

  this.setState({phone: event.target.value})
  localStorage.setItem("phone", event.target.value);
}


  render(){

     return (
       <div>
               <Typography variant="h6" gutterBottom>
                    Payment method
               </Typography>
               <Grid container spacing={3}>
                    <Grid item xs={12} md={6}>
                         <TextField
                              required
                              id="address"
                              label="Enter payment address"
                              fullWidth
                              autoComplete="address"
                              value={this.state.value}
                              onChange={this.handleChange}


                         />
                    </Grid>
                    <Grid item xs={12} md={6}>
                         <TextField
                              required
                              id="fakeCreditCard"
                              label="Please enter a fake card number"
                              fullWidth
                              autoComplete="cc-number"
                              value={this.state.value}
                              onChange={this.handleCard}

                          />
                    </Grid>
                    <Grid item xs={12} md={6}>
                         <TextField
                              required
                              id="phone"
                              label="Phone number"
                              fullWidth
                              autoComplete="phone"
                              value={this.state.value}
                              onChange={this.handlePhone}

                         />
                    </Grid>
                    <Grid item xs={12} md={6}>
                         <TextField
                              required
                              id="requests"
                              label="Enter any requests"
                              helperText="Any delivery notes?"
                              fullWidth
                              autoComplete="requests"

                         />
                    </Grid>
                    <Grid item xs={12}>
                         <hr />

                    </Grid>
               </Grid>
               </div>
             );
           }

}
