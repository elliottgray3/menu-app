import React, { Component } from "react";
import {
     makeStyles,
     Typography,
     List,
     ListItem,
     ListItemText,
} from "@material-ui/core";
import instance from "../components/axios";
import { cookies } from "../components/Cookies";

var total = parseFloat(cookies.get("totalprice")).toFixed(2);

export default class Review extends Component {
     state = {
          items: [],
          isLoading: true,
          errors: null,
     };

     componentDidMount() {
          instance
               .post("fetchCart", {
                    username: localStorage.getItem("username"),
               })

               .then((response) =>
                    response.data.foodItems.map((item) => ({
                         name: `${item.name}`,
                         price: `${item.price}`,
                         quantity: `${item.quantity}`,
                    }))
               )

               .then((items) => {
                    this.setState({
                         items,
                         isLoading: false,
                    });
               })
               .catch((error) => this.setState({ error, isLoading: false }));
     }

     render() {
          const { items } = this.state;
          return (
               <div>
                    <Typography variant="h6" gutterBottom>
                         Order summary
                    </Typography>
                    <List disablePadding>
                         {items.map((item) => {
                              const { name, price, quantity } = item;
                              return (
                                   <ListItem key={name}>
                                        <ListItemText
                                             primary={name}
                                             secondary={" x" + quantity}
                                        />
                                        <Typography variant="body2">
                                             {"$" + price + ".00"}
                                        </Typography>
                                   </ListItem>
                              );
                         })}

                         <ListItem>
                              <ListItemText primary="Total" />
                              <Typography variant="subtitle1">
                                   {"$" + total}
                              </Typography>
                         </ListItem>
                    </List>
               </div>
          );
     }
}
