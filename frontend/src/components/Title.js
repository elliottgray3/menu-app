import React, { Component } from "react";
import CurrentURL from "../pages/AccountVerification";
export default class Title extends Component {
     render() {
          function CurrentURL() {
               var urly = window.location.href;
               var token = urly.split("/");

               if (token[3] === "AccountVerification") {
                    return "verify";
               } else if (token[3] === "PasswordVerification") {
                    return "verifyPasswordReset";
               }
          }
          if (CurrentURL() == "verifyPasswordReset") {
               return (
                    <div>
                         <br />
                         <h1>Password Reset</h1>
                         <hr />
                    </div>
               );
          } else if (CurrentURL() == "verify") {
               return (
                    <div>
                         <br />
                         <h1>Email Verify</h1>
                         <hr />
                    </div>
               );
          }
     }
}
