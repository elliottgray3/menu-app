import React from "react";
import * as Yup from "yup";
import instance from "../components/axios";
import { withFormik } from "formik";
import {
     withStyles,
     Card,
     CardContent,
     CardActions,
     TextField,
     Button,
     Container,
} from "@material-ui/core";

const styles = () => ({
     card: {
          maxWidth: 420,
          marginTop: 50,
     },
     container: {
          display: "Flex",
          justifyContent: "center",
     },
     actions: {
          float: "right",
     },
});

const form = (props) => {
     const {
          classes,
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit,
          handleReset,
     } = props;

     return (
          <div className={classes.container}>
               <form onSubmit={handleSubmit}>
                    <Card className={classes.card}>
                         <br />
                         <h1>Password Reset</h1>
                         <hr />
                         <CardContent>
                              <TextField
                                   id="Password"
                                   label="Password"
                                   type="Password"
                                   value={values.Password}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   helperText={
                                        touched.Password ? errors.Password : ""
                                   }
                                   error={
                                        touched.newPassword &&
                                        Boolean(errors.Password)
                                   }
                                   margin="dense"
                                   variant="outlined"
                                   fullWidth
                              />
                              <TextField
                                   id="confirmPassword"
                                   label="Confirm Password"
                                   type="password"
                                   value={values.confirmPassword}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   helperText={
                                        touched.confirmPassword
                                             ? errors.confirmPassword
                                             : ""
                                   }
                                   error={
                                        touched.confirmPassword &&
                                        Boolean(errors.confirmPassword)
                                   }
                                   margin="dense"
                                   variant="outlined"
                                   fullWidth
                              />
                         </CardContent>
                         <Container>
                              <CardActions className={classes.actions}>
                                   <Button
                                        type="submit"
                                        color="primary"
                                        disabled={isSubmitting}
                                   >
                                        SUBMIT
                                   </Button>
                                   <Button
                                        color="secondary"
                                        onClick={handleReset}
                                   >
                                        CLEAR
                                   </Button>
                              </CardActions>
                         </Container>
                         <br />
                    </Card>
               </form>
          </div>
     );
};

const PasswordReset = withFormik({
     mapPropsToValues: ({ Password, confirmPassword }) => {
          return {
               newPassword: Password || "",
               confirmPassword: confirmPassword || "",
          };
     },

     validationSchema: Yup.object().shape({
          Password: Yup.string()
               .min(8, "Password must contain at least 8 characters")
               .required("Enter your password"),
          confirmPassword: Yup.string()
               .required("Confirm your password")
               .oneOf([Yup.ref("Password")], "Password does not match"),
     }),

     handleSubmit: (values, { setSubmitting }) => {
          setTimeout(() => {
               instance
                    .post("verifyPasswordReset", {
                         newPassword: values.Password,
                    })
                    .then(function (response) {
                         console.log(response);
                         if (response.data.error === "") {
                              window.location.href = "/login";
                              alert("Password Updated");
                         } else {
                              console.log("Error:", response.data.error);
                              alert(response.data.error);
                         }
                    });
               setSubmitting(false);
          });
     },
})(form);

export default withStyles(styles)(PasswordReset);
