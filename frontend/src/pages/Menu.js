import React, { Component } from "react";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import { cookies } from "../components/Cookies";
import instance from "../components/axios";

import {
     Container,
     CardActions,
     Card,
     CardContent,
     CardMedia,
     CardHeader,
     Box,
     IconButton,
     Typography,
     Tooltip,
} from "@material-ui/core";

export default class Menu extends Component {
     state = {
          quantity: 0,
          items: [],
          isLoading: true,
          errors: null,
          count: 0,
     };

     componentDidMount() {
          instance
               .post("searchInventory", {
                    name: "",
               })

               .then((response) =>
                    response.data.results.map((item) => ({
                         name: `${item.name}`,
                         description: `${item.description}`,
                         picture: `${item.picture}`,
                         price: `${item.price}`,
                         nutritionInfo: `${item.nutritionInfo}`,
                         waitTime: `${item.waitTime}`,
                         quantity: `${item.quantity}`,
                         id: `${item.id}`,
                    }))
               )

               .then((items) => {
                    console.log(items);
                    this.setState({
                         items,
                         isLoading: false,
                    });
               })
               .catch((error) => this.setState({ error, isLoading: false }));
     }

     addItem = (item) => {
          
          this.setState({
               quantity: this.state.quantity + 1,
          });
          cookies.set("currentItemAdding", this.state.quantity + 1);

          if (localStorage.getItem("username") === null) {
               window.location.href = "/LoginAccount";
          } else {
               // Removing $ from the price for backend processing
               var originalPrice = item.price;
               
               var newPrice = parseFloat(item.price.slice(1));
               

               var total = cookies.get("totalprice");
               
               if (total === undefined || total === "NaN") {
                    total = 0;
               }
               total = parseFloat(total) + parseFloat(newPrice);

               
               cookies.set("totalprice", total);
                // Pacman

               // Convert string values that are supposed to be number values to numbers
               var wait = Number(item.waitTime);
               console.log(item.price);
               var price = Number(item.price.slice(1));
               console.log(price);

               // Reset item.price to original to prevent it from cutting down the number
               item.price = originalPrice;

               instance
                    .post("addItem", {
                         username: localStorage.getItem("username"),
                         name: item.name,
                         price: price,
                         waitTime: wait,
                         nutritionInfo: item.nutritionInfo,
                    })

                    .then(function (response) {
                         console.log(response);
                    });
          }
     };
     deleteItem = (item) => {
          if (this.state.quantity <= 0) {
               this.setState({
                    quantity: 0,
               });
               cookies.set("currentItemAdding", 0);
          } else {
               this.setState({
                    quantity: this.state.quantity - 1,
               });
               cookies.set("currentItemAdding", this.state.quantity - 1);
          }
          var originalPrice = item.price;
         
          var newPrice = parseFloat(item.price.slice(1));
          

          var total = cookies.get("totalprice");
          
          if (total === undefined) {
               total = "0";
          }
          if (!(total <= 0)) total = parseFloat(total) - parseFloat(newPrice);

          
          cookies.set("totalprice", total);
           // Pacman

          // Convert string values that are supposed to be number values to numbers
          var wait = Number(item.waitTime);
          console.log(item.price);
          var price = Number(item.price.slice(1));
          console.log(price);

          // Reset item.price to original to prevent it from cutting down the number
          item.price = originalPrice;

          instance
               .post("deleteItem", {
                    username: localStorage.getItem("username"),
                    name: item.name,
                    price: price,
                    waitTime: wait,
                    nutritionInfo: item.nutritionInfo,
               })

               .then(function () {
                    // window.location.reload();
               });
     };

     render() {
          const { isLoading, items } = this.state;

          return (
               <div>
                    <h1>Menu </h1>
                    {!isLoading ? (
                         items.map((item) => {
                              const {
                                   name,
                                   description,
                                   picture,
                                   price,
                                   nutritionInfo,
                                   quantity,
                                   id,
                              } = item;

                              return (
                                   <div key={name}>
                                        <Container
                                             style={{
                                                  alignContent: "center",
                                                  minWidth: "450px",
                                                  maxWidth: "60%",
                                             }}
                                        >
                                             <Card>
                                                  <CardHeader
                                                       style={{
                                                            textAlign: "center",
                                                       }}
                                                       title={name}
                                                  />
                                                  <CardMedia
                                                       style={{
                                                            maxBlockSize:
                                                                 "450px",

                                                            paddingBottom:
                                                                 "56.25%", // 16:9
                                                       }}
                                                       image={picture}
                                                  />
                                                  <CardContent>
                                                       <Typography
                                                            variant="body1"
                                                            color="textSecondary"
                                                            component="p"
                                                       >
                                                            {description}
                                                       </Typography>
                                                       <br />
                                                       <Box>
                                                            <Typography
                                                                 variant="body2"
                                                                 color="textSecondary"
                                                                 component="p"
                                                            >
                                                                 {nutritionInfo}
                                                            </Typography>
                                                       </Box>
                                                  </CardContent>

                                                  <CardActions
                                                       disableSpacing
                                                       onClick={this.addItem.bind(
                                                            null,
                                                            item
                                                       )}
                                                  >
                                                       <Tooltip
                                                            title="Add Item to cart"
                                                            arrow={true}
                                                            placement="left"
                                                       >
                                                            <IconButton
                                                                 id={id}
                                                                 aria-label="add to cart"
                                                                 color="inherit"
                                                            >
                                                                 <AddShoppingCartIcon />
                                                                 <Typography
                                                                      variant="h5"
                                                                      color="textPrimary"
                                                                 >
                                                                      {price}
                                                                 </Typography>
                                                            </IconButton>
                                                       </Tooltip>
                                                  </CardActions>
                                             </Card>
                                        </Container>
                                        <br />
                                   </div>
                              );
                         })
                    ) : (
                         <p>Loading...</p>
                    )}
               </div>
          );
     }
}
