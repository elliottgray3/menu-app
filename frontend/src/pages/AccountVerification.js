import React from "react";
import instance from "../components/axios";
import { withFormik } from "formik";
import Title from "../components/Title";
import TheField from "../components/TheField";
import {
     withStyles,
     Card,
     CardContent,
     CardActions,
     TextField,
     Button,
     Container,
} from "@material-ui/core";

function CurrentURL() {
     var urly = window.location.href;
     var token = urly.split("/");
     if (token[3] === "AccountVerification") {
          return "verify";
     } else if (token[3] === "PasswordVerification") {
          return "verifyPasswordReset";
     }
}
const styles = () => ({
     card: {
          maxWidth: 420,
          minWidth: 375,
          marginTop: 50,
     },
     container: {
          display: "Flex",
          justifyContent: "center",
     },
     actions: {
          float: "right",
     },
});

const form = (props) => {
     const {
          classes,
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit,
     } = props;

     return (
          <div>
               <form onSubmit={handleSubmit}>
                    <Card className={classes.card}>
                         <Title />
                         <CardContent>
                              <TheField id="field" />
                              <TextField
                                   id="key"
                                   label="Generated Key"
                                   value={values.key}
                                   onChange={handleChange}
                                   onBlur={handleBlur}
                                   helperText={touched.key ? errors.key : ""}
                                   error={
                                        touched.username && Boolean(errors.key)
                                   }
                                   margin="dense"
                                   variant="outlined"
                                   fullWidth
                              />
                         </CardContent>
                         <Container>
                              <CardActions className={classes.actions}>
                                   <Button
                                        type="submit"
                                        color="primary"
                                        disabled={isSubmitting}
                                   >
                                        SUBMIT
                                   </Button>
                              </CardActions>
                         </Container>
                    </Card>
               </form>
          </div>
     );
};

const AccountVerification = withFormik({
     mapPropsToValues: ({ key }) => {
          return {
               key: key || "",
          };
     },

     handleSubmit: (values, { setSubmitting }) => {
          setTimeout(() => {
               const postHere = CurrentURL();
               alert(values);
               instance
                    .post(postHere, {
                         verificationString: values.key,
                         newPassword: TheField.values.newPassword,
                    })
                    .then(function (response) {
                         console.log(response);
                         window.location.href = "/LoginAccount";
                    });
               console.log("Your key submission", values);
               setSubmitting(false);
          });
     },
})(form);

export default withStyles(styles)(AccountVerification);
