import React, { Component } from "react";
import instance from "../components/axios";

import BackspaceIcon from "@material-ui/icons/Backspace";
import IconButton from "@material-ui/core/IconButton";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import Cookies from "universal-cookie";

export default class Cart extends Component {
     state = {
          items: [],
          isLoading: true,
          errors: null,
          count: 0,
     };

     increment = () => {
          this.setState({
               count: this.state.count + 1,
          });
     };

     decrement = () => {
          if (this.state.count <= 0) {
               this.setState({
                    count: 0,
               });
          } else {
               this.setState({
                    count: this.state.count - 1,
               });
          }
     };
     componentDidMount() {
          instance
               .post("fetchCart", {
                    username: localStorage.getItem("username"),
               })

               .then((response) =>
                    response.data.foodItems.map((item) => ({
                         name: `${item.name}`,
                         price: `${item.price}`,
                         nutritionInfo: `${item.nutritionInfo}`,
                         waitTime: `${item.waitTime}`,
                         quantity: `${item.quantity}`,
                    }))
               )

               .then((items) => {
                    this.setState({
                         items,
                         isLoading: false,
                    });
               })
               .catch((error) => this.setState({ error, isLoading: false }));
     }

     deleteItem = (item) => {
          // Removing $ from the price for backend processing
          var originalPrice = item.price;

          item.price = item.price.slice(1);

          // Convert string values that are supposed to be number values to numbers
          var wait = Number(item.waitTime);
          var price = Number(item.price);

          // Reset item.price to original to prevent it from cutting down the number
          item.price = originalPrice;

          instance
               .post("deleteItem", {
                    username: localStorage.getItem("username"),
                    name: item.name,
                    price: price,
                    waitTime: wait,
                    nutritionInfo: item.nutritionInfo,
               })

               .then(function () {
                    window.location.reload();
               });
     };
     addItem = (item) => {
          if (localStorage.getItem("username") === null) {
               window.location.href = "/LoginAccount";
          } else {
               // Removing $ from the price for backend processing
               var originalPrice = item.price;

               var newPrice = parseFloat(item.price.slice(1));

               const cookies = new Cookies();
               var total = cookies.get("totalprice");

               if (total === undefined) {
                    total = 0;
               }
               total = parseFloat(total) + parseFloat(newPrice);

               cookies.set("totalprice", total);

               // Convert string values that are supposed to be number values to numbers

               console.log(item.price);
               var price = Number(item.price.slice(1));
               console.log(price);

               // Reset item.price to original to prevent it from cutting down the number
               item.price = originalPrice;

               instance
                    .post("addItem", {
                         username: localStorage.getItem("username"),
                         name: item.name,
                         price: item.price,
                    })

                    .then(function (response) {
                         console.log(response);
                    });
          }
     };

     render() {
          const { isLoading, items } = this.state;
          return (
               <div>
                    <br />
                    <h1>Cart Page (Pre checkout)</h1>

                    {!isLoading ? (
                         items.map((item) => {
                              const { name, price, quantity } = item;
                              return (
                                   <div key={name}>
                                        {name}
                                        Price: {"$" + price}
                                        Quantity: {quantity}x2
                                        <IconButton aria-label="Remove from cart">
                                             <BackspaceIcon
                                                  onClick={this.deleteItem.bind(
                                                       null,
                                                       item
                                                  )}
                                             />
                                        </IconButton>
                                        <IconButton aria-label="add to cart">
                                             <AddShoppingCartIcon
                                                  onClick={this.addItem.bind(
                                                       null,
                                                       item
                                                  )}
                                             />
                                        </IconButton>
                                   </div>
                              );
                         })
                    ) : (
                         <p>Loading...</p>
                    )}
               </div>
          );
     }
}
