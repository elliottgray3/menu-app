import React, { Component } from "react";
import instance from "../components/axios";
import { Card } from "react-bootstrap";
import BackspaceIcon from "@material-ui/icons/Backspace";
import IconButton from "@material-ui/core/IconButton";
import { Button } from "react-bootstrap";

const checkOutStyle = {
     marginTop: "10px",
     marginRight: "10px",
};

export default class Cart extends Component {
     state = {
          items: [],
          isLoading: true,
          errors: null,
     };

     componentDidMount() {
          instance
               .post("fetchCart", {
                    username: localStorage.getItem("username"),
               })

               .then((response) =>
                    response.data.foodItems.map((item) => ({
                         name: `${item.name}`,
                         price: `${item.price}`,
                         nutritionInfo: `${item.nutritionInfo}`,
                         waitTime: `${item.waitTime}`,
                         quantity: `${item.quantity}`,
                    }))
               )

               .then((items) => {
                    this.setState({
                         items,
                         isLoading: false,
                    });
               })
               .catch((error) => this.setState({ error, isLoading: false }));
     }

     deleteItem = (item) => {
          // Removing $ from the price for backend processing
          var originalPrice = item.price;

          item.price = item.price.slice(1);

          // Convert string values that are supposed to be number values to numbers
          var wait = Number(item.waitTime);
          var price = Number(item.price);

          // Reset item.price to original to prevent it from cutting down the number
          item.price = originalPrice;

          instance
               .post("deleteItem", {
                    username: localStorage.getItem("username"),
                    name: item.name,
                    price: price,
                    waitTime: wait,
                    nutritionInfo: item.nutritionInfo,
               })

               .then(function () {
                    window.location.reload();
               });
     };

     render() {
          function handleClick() {
               if (localStorage.getItem("username") === null) {
                    window.location.href = "/LoginAccount";
               } else {
                    window.location.href = "/checkout";
               }
          }

          const { isLoading, items } = this.state;
          return (
               <div>
                    <br />
                    <Button
                         variant="danger"
                         size="lg"
                         className="float-right"
                         style={checkOutStyle}
                         onClick={handleClick}
                    >
                         Checkout
                    </Button>
                    <h1>Cart Page (Pre checkout)</h1>

                    <hr />

                    {!isLoading ? (
                         items.map((item) => {
                              const {
                                   name,
                                   price,
                                   nutritionInfo,
                                   waitTime,
                                   quantity,
                              } = item;

                              return (
                                   <div key={name}>
                                        <Card>
                                             <Card.Body>
                                                  <Card.Title>
                                                       {name}
                                                  </Card.Title>
                                                  <Card.Text>
                                                       {nutritionInfo}
                                                  </Card.Text>
                                                  <Card.Text>
                                                       WaitTime:{" "}
                                                       {waitTime + " Minutes"}
                                                  </Card.Text>
                                                  <Card.Text>
                                                       Price:{" "}
                                                       {"$" + price + ".00"}
                                                  </Card.Text>
                                                  <Card.Text>
                                                       Quantity:{" "}
                                                       {"x" + quantity}
                                                  </Card.Text>

                                                  <IconButton aria-label="Remove from cart">
                                                       <BackspaceIcon
                                                            onClick={this.deleteItem.bind(
                                                                 null,
                                                                 item
                                                            )}
                                                       />
                                                  </IconButton>
                                             </Card.Body>
                                        </Card>
                                   </div>
                              );
                         })
                    ) : (
                         <p>Loading...</p>
                    )}
               </div>
          );
     }
}
