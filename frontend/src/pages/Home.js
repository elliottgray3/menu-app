import "./Home.css";
import React, { Component } from "react";
import Welcome from '../Sur_la_Table_Welcome.png'

export default class Home extends Component {
     render() {
          return (
               <div>
                    <br />
                    <h1>Welcome to</h1>
                    <hr />
                    <br />
                    <img src={Welcome} alt="Welcome Pic"/>
               </div>
          );
     }
}
