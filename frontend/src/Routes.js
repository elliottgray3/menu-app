import React from "react";
import Menu from "./pages/Menu";
import Home from "./pages/Home";
import Cart from "./pages/Ca1rt";
import Checkout from "./components/Checkout";
import LoginForm from "./components/LoginForm";
import AccountVerification from "./pages/AccountVerification";
import PasswordReset from "./pages/PasswordReset";
import { Route, Switch } from "react-router-dom";
import RegisterForm from "./components/RegisterForm";
import ForgotPassword from "./components/ForgotPassword";
import { Fade } from "@material-ui/core";
export default function Routes() {
     return (
          <Fade>
               <Switch>
                    <Route exact path="/">
                         <Home />
                    </Route>
                    <Route exact path="/RegisterAccount">
                         <RegisterForm />
                    </Route>
                    <Route exact path="/LoginAccount">
                         <LoginForm />
                    </Route>
                    <Route exact path="/forgotPassword">
                         <ForgotPassword />
                    </Route>
                    <Route exact path="/Menu">
                         <Menu />
                    </Route>
                    <Route exact path="/AccountVerification">
                         <AccountVerification />
                    </Route>
                    <Route exact path="/PasswordVerification">
                         <AccountVerification />
                    </Route>
                    <Route exact path="/Cart">
                         <Cart />
                    </Route>
                    <Route exact path="/Checkout">
                         <Checkout />
                    </Route>
                    <Route exact path="/PasswordReset">
                         <PasswordReset />
                    </Route>
               </Switch>
          </Fade>
     );
}
